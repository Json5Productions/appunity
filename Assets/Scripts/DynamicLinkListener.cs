﻿using Firebase.DynamicLinks;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class DynamicLinkListener : MonoBehaviour
{
    public event Action<int> OnItemRequested;

    public static DynamicLinkListener Instance
    {
        get;
        private set;
    }

    private void Awake()
    {
        Instance = this;

        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            var dependencyStatus = task.Result;

            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                DynamicLinks.DynamicLinkReceived += OnDynamicLink;
            }
            else
            {
                Debug.LogError(System.String.Format(
                 "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                // Firebase Unity SDK is not safe to use here.
            }
        });
    }

    private void OnDynamicLink(object sender, EventArgs args)
    {
        ReceivedDynamicLinkEventArgs dynamicLinkEventArgs = args as ReceivedDynamicLinkEventArgs;
        string[] parameterValues = dynamicLinkEventArgs.ReceivedDynamicLink.Url.Query.Split('=');

        if (parameterValues.Length > 1)
        {
            if (OnItemRequested != null)
                OnItemRequested(int.Parse(parameterValues[1]));

            Debug.Log(int.Parse(parameterValues[1]));
        }
    }

    [EasyButtons.Button]
    private void CreateDynamicLink()
    {
        StartCoroutine(CreateDynamicLinkCoroutine());
    }

    private IEnumerator CreateDynamicLinkCoroutine()
    {
        WWWForm form = new WWWForm();
        form.AddField("longDynamicLink", string.Format("https://bikebi.page.link/?link=https://bikebi.com.co/producto?id={0}&apn=com.vrc.bikebi&ibi=com.vrc.bikebi", 200));
        UnityWebRequest webRequest = UnityWebRequest.Post("https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=AIzaSyDrHfN2tiepQyogztmcKvYE3zgT6nND1wI", form);
        yield return webRequest.SendWebRequest();

        if (string.IsNullOrEmpty(webRequest.error))
        {
            Debug.Log(webRequest.downloadHandler.text);
        }
        else
            Debug.LogError(webRequest.error.ToString());
    }
}