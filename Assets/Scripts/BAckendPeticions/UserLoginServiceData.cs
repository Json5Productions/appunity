﻿using SnowKore.Services;
using System.Collections.Generic;
using UnityEngine;

public class UserLoginServiceData : NewServiceData
{
    private string email;

    public UserLoginServiceData(string email)
    {
        this.email = email;
    }

    protected override Dictionary<string, object> Body
    {
        get
        {
            Dictionary<string, object> body = new Dictionary<string, object>();
            return body;
        }
    }
    protected override string ServiceURL { get => $"restapi/v1/usuarioRegistro?user={email}"; }
    protected override Dictionary<string, object> Params
    {
        get
        {
            Dictionary<string, object> retHeaders = new Dictionary<string, object>();
            return retHeaders;
        }
    }

    protected override Dictionary<string, string> Headers
    {
        get
        {
            Dictionary<string, string> retHeaders = new Dictionary<string, string>();
            retHeaders.Add("Content-Type", "application/x-www-form-urlencoded");
            retHeaders.Add("Authorization", "3d524a53c110e4c22463b10ed32cef9d");
            return retHeaders;
        }
    }

    protected override ServiceType ServiceType => ServiceType.POST;
}
