﻿using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.Experimental.XR;
using UnityEngine;
using System.Collections.Generic;
using System;

[RequireComponent(typeof(ARRaycastManager))]
public class ArTapToPlace : MonoBehaviour
{
    [SerializeField]
    private AssetBundleInstantiatorExample assetBundle;
    [SerializeField]
    private GameObject indicator;
    private GameObject spawnedObject;
    [SerializeField]
    private ARRaycastManager raycastManager;
    [SerializeField]
    private RectTransform touchArea;
    [SerializeField]
    private Transform parentAR;
    [SerializeField]
    private Pose arPlacement;
    [SerializeField]
    private Camera cameraAR;
    [SerializeField]
    private ModeAppManager mode;

    private bool poseValid = false, trackPlace = false;
    public float rotatespeed = 150f;
    private float _startingPosition;
    public void ResetAR()
    {
        assetBundle.InstancedObjectBundle.transform.parent = parentAR;
        parentAR.rotation = Quaternion.Euler(0,-90,0);
        trackPlace = true;
        spawnedObject = null;
    }

    void Update()
    {
        if (trackPlace)
            UpdatePlacementAr();
        UPdatePlacementIndicator();
        if (poseValid)
            InstantiateObject();
    }

    private void UPdatePlacementIndicator()
    {
        if (poseValid && trackPlace)
        {
            indicator.SetActive(true);
            indicator.transform.SetPositionAndRotation(arPlacement.position, arPlacement.rotation);
        }
        else
        {
            indicator.SetActive(false);
        }
    }

    private void InstantiateObject()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (RectTransformUtility.RectangleContainsScreenPoint(touchArea, touch.position))
            {
                if (spawnedObject == null)
                {
                    RePosAR();
                }
                else
                {
                    switch (touch.phase)
                    {
                        case TouchPhase.Began:
                            _startingPosition = touch.position.x;
                            break;
                        case TouchPhase.Moved:
                            if (_startingPosition > touch.position.x)
                            {
                                spawnedObject.transform.Rotate(Vector3.up, rotatespeed * Time.deltaTime);
                            }
                            else if (_startingPosition < touch.position.x)
                            {
                                spawnedObject.transform.Rotate(Vector3.up, rotatespeed * -Time.deltaTime);
                            }
                            break;
                        case TouchPhase.Ended:
                            Debug.Log("Touch Phase Ended.");
                            break;
                    }

                }
            }           
        }
    }

    public void RePosAR ()
    {
        if (mode.CurrentMode == ModeApp.Ar)
        {
            assetBundle.InstancedObjectBundle.transform.parent = parentAR;
                assetBundle.InstancedObjectBundle.transform.localPosition = Vector3.zero;
            spawnedObject = assetBundle.InstancedObjectBundle.transform.parent.gameObject;
            spawnedObject.transform.position = arPlacement.position + new Vector3(0, 0.367f, 0.456f);
            spawnedObject.transform.localScale = Vector3.one;
            spawnedObject.transform.rotation = arPlacement.rotation;
            assetBundle.InstancedObjectBundle.SetActive(true);
            trackPlace = false;
        }
    }

    private void UpdatePlacementAr()
    {
        var screenCenter = Camera.main.ViewportToScreenPoint(new Vector3(.5f, .5f));
        var hits = new List<ARRaycastHit>();
        raycastManager.Raycast(screenCenter, hits, TrackableType.Planes);
        poseValid = hits.Count > 0;
        if (poseValid)
        {
            arPlacement = hits[0].pose;
        }
    }
}
