﻿using System;
using System.Collections.Generic;

[Serializable]
public class ResponseProducts
{
    public bool error;
    public int categoria;
    public List<int> subcategoria;
    public string search;
    public int offset;
    public int limit;
    public int total;
    public int totalNotFiltered;
    public List<Product> rows;
}
[Serializable]
public class Product
{
    public int id;
    public string referencia1;
    public string referencia2;
    public string referencia3;
    public int tienda;
    public int categoria;
    public string marca;
    public string producto;
    public string descripcion;
    public string garantia;
    public int existencias;
    public string modelo;
    public int agotado;
    public string valor;
    public string moneda;
    public string valorantes;
    public string iva;
    public string ivatipo;
    public string dimlargo;
    public string dimancho;
    public string dimalto;
    public string dimpesofisico;
    public int attrnuevo;
    public int attrpromo;
    public int attrpedido;
    public int attrdestacado;
    public int attrapp;
    public string attrGUID;
    public string videoembed0;
    public string videoembed1;
    public string videoembed2;
    public int likes;
    public int activo;
    public int vistas;
    public string actualizado;
    public Imagenes imagenes;
}