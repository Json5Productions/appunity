﻿using System;
using System.Collections.Generic;

[Serializable]
public class ResponseSpecificProduct
{
    public bool error ;
    public Imagenes imagenes ;
    public string id ;
    public Product row ;
}
[Serializable]
public class Imagenes
{
    public string origina ;
    public string web ;
    public List<string> imagenes = new List<string>();
}