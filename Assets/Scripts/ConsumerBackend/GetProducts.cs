﻿using EasyButtons;
using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetProducts : MonoBehaviour
{
    [SerializeField]
    private Transform ContainerDestacados;
    [SerializeField]
    private List<PrefabContainerMiniItem> prefabContainerMiniItem;
    [SerializeField]
    private PrefabDestacados prefabDestacados;
    [SerializeField]
    private ResponseProducts response,responseDestacados;
    [Button]
    public void GetProductsPetitionBicisMini(string Cat)
    {
        string categoria = "BICICLETAS";
        if (!string.IsNullOrEmpty(Cat))
        {
            categoria = Cat;
        }
        StartCoroutine(DoPeticionToBack(categoria));
    }
    [Button]
    public void GetProductsDestacados()
    {
        StartCoroutine(DoPeticionToBackDestacados());
    }
    private IEnumerator DoPeticionToBack(string categoria)
    {
        string cat = $"categoria={GetCategories.Instance.ReturnIndexCategoriebyName(categoria)}";
        yield return new ProductosServiceData(cat).SendAsync(Response);
    }
    private IEnumerator DoPeticionToBackDestacados()
    {
        yield return new DestacadosServiceData().SendAsync(ResponseDestacados);
    }

    public void AddFavoriteItem (int id)
    {

    }

    private void ResponseDestacados(string response, string code)
    {
        if (code.Contains("200"))
        {
            this.responseDestacados = JsonConvert.DeserializeObject<ResponseProducts>(response);
            InstantiateDestacados();
        }
    }
    private void Response(string response, string code)
    {
        if (code.Contains("200"))
        {
            this.response = JsonConvert.DeserializeObject<ResponseProducts>(response);
            InstantiateMiniBicis();
        }
    }

    public void InstantiateDestacados()
    {
        foreach (Transform t in ContainerDestacados)
        {
            Destroy(t.gameObject);
        }

        var limit = 0;
        if (responseDestacados.rows.Count <= 5)
        {
            limit = responseDestacados.rows.Count;
        }
        else
            limit = 5;
        for (int i = 0; i < limit; i++)
        {
            var item = Instantiate(prefabDestacados, ContainerDestacados);
            item.Product = responseDestacados.rows[i];
            item.SetupPrefab(responseDestacados.rows[i].imagenes.imagenes[0]);
        }
    }
    public void InstantiateMiniBicis()
    {
        var limit = 0;
        if (response.rows.Count <= 5)
        {
            limit = response.rows.Count;
        }
        else
            limit = 5;

        for (int i = 0; i < limit; i++)
        {
            prefabContainerMiniItem[i].ProductData = response.rows[i];
            prefabContainerMiniItem[i].SetupItem(response.rows[i].producto, GetCategories.Instance.ReturnNameCategoriebyIndex(response.rows[i].categoria),
            response.rows[i].marca, response.rows[i].valor, response.rows[i].imagenes.imagenes[0]);
        }
    }
}
