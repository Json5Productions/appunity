﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

public class GetVersion : MonoBehaviour
{
    [SerializeField]
    private string urlData;
    [SerializeField]
    private string Version;

    public void GetVersionServer()
    {
        StartCoroutine(DownlaodData());
    }

    IEnumerator DownlaodData()
    {
        UnityWebRequest www = UnityWebRequest.Get(urlData);
        yield return www.SendWebRequest();

        if (string.IsNullOrEmpty(www.error))
        {
            Version =www.downloadHandler.text;
            VersionReceived();
        }
    }

    private void VersionReceived ()
    {
        if (Version != Application.version)
        {
            WarningManager.Instance.ShowWarning("Para continuar debes actualizar la aplicación", () => Application.OpenURL(DecideActionByDevice())); ;

        }
        else
        {
            Debug.Log("version IGUAL");
        }
    }

    private string DecideActionByDevice ()
    {
#if UNITY_ANDROID
       return "https://play.google.com/store/apps/details?id=com.vrc.bikebi&hl=es&gl=US";
#else
       return "https://apps.apple.com/co/app/bikebi-3d/id1543794228";

#endif
    }

}
