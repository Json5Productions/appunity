﻿using EasyButtons;
using Newtonsoft.Json;
using System.Collections;
using RestSharp.Contrib;
using UnityEngine;

public class GetCategories : MonoBehaviour
{
    [SerializeField]
    private ResponseCategories response;
    public ResponseCategories Response { get => response; set => response = value; }
    public static GetCategories Instance;

    private void Awake()
    {
        if (GetCategories.Instance == null)
            GetCategories.Instance = this;
        else
            Destroy(this) ;
    }
    private void Start()
    {
        GetProductsPetition();
    }
    [Button]
    public void GetProductsPetition()
    {
        StartCoroutine(DoPeticionToBack());
    }
    private IEnumerator DoPeticionToBack()
    {
        yield return new CategoriasInfoServiceData().SendAsync(ResponseBack);
    }
    private void ResponseBack(string response, string code)
    {
        if (code.Contains("200"))
        {
            this.Response = JsonConvert.DeserializeObject<ResponseCategories>(response);
        }
    }

    public string ReturnNameCategoriebyIndex (int catIndex)
    {
        var categorieName = Response.rows.Find(x=>x.id == catIndex);
        if (categorieName != null)
            return categorieName.nombre;
        else
            return "NO CATEGORIA";
    }
    public int ReturnIndexCategoriebyName(string namecat)
    {
        foreach(var name in Response.rows)
        {
            name.nombre = HttpUtility.HtmlDecode(name.nombre);
        }

        var categorieName = Response.rows.Find(x=>x.nombre == namecat);
        if (categorieName == null)
            categorieName = Response.rows.Find(x => x.nombre == namecat.ToUpper());

        if (categorieName != null)
            return categorieName.id;
        else
            return -1;
    }
}
