﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ResponseCategories
{
    public bool error ;
    public string search ;
    public int offset ;
    public int limit ;
    public int total ;
    public int totalNotFiltered ;
    public List<Categorie> rows ;
}
[System.Serializable]
public class Categorie
{
    public int id ;
    public string nombre ;
    public int nodosHijos ;
    public int nodoPadre ;
}