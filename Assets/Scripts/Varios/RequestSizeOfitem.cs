﻿using DoozyUI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RequestSizeOfitem : MonoBehaviour
{
    public UnityEvent onArButton, on3dButton;
    [SerializeField]
    private AssetBundleDownloader assetBundle;

    private bool clickonArButton = false;

    public bool ClickonArButton { get => clickonArButton; set => clickonArButton = value; }

    public void AcceptDownload ()
    {
        if (ClickonArButton)
        {
            onArButton.Invoke();
        }
        else
        {
            on3dButton.Invoke();
        }
        HideElemets();
    }
    public void AcceptDownload (string no)
    {
        if (ClickonArButton)
        {
            onArButton.Invoke();
        }
        else
        {
            on3dButton.Invoke();
        }
    }
    void HideElemets ()
    {
        UINavigation.HideUiElement("BackgroundLogin", "StartScreens", false);
        UINavigation.HideUiElement("BicisScreen", "StartScreens", false);
        UINavigation.HideUiElement("FavScreen", "StartScreens", false);
        UINavigation.HideUiElement("Inicio", "StartScreens", false);
        UINavigation.HideUiElement("ItemScreen", "StartScreens", false);
        UINavigation.HideUiElement("ItemsList", "StartScreens", false);
        UINavigation.HideUiElement("ItemsStoreScreen", "StartScreens", false);
        UINavigation.HideUiElement("login", "StartScreens", false);
        UINavigation.HideUiElement("Menudistorsion", "StartScreens", false);
        UINavigation.HideUiElement("menuInferior", "StartScreens", false);
        UINavigation.HideUiElement("menuLateral", "StartScreens", false);
        UINavigation.HideUiElement("MiBiciScreen", "StartScreens", false);
        UINavigation.HideUiElement("Perfil", "StartScreens", false);
        UINavigation.HideUiElement("StartMiniScreenAll", "StartScreens", false);
        UINavigation.HideUiElement("start", "StartScreens", false);


    }
    public void RequestSize()
    {
        if (!assetBundle.AlreadyHaveBundle(ScreenStoreItemSelected.Instace.CurrentSpecificProduct.row.id))
        {
            AssetBundleManager.Instance.GetFileSize();
        }else {
            AcceptDownload();
        }
    }

}
