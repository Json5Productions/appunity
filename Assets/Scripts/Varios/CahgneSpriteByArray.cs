﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CahgneSpriteByArray : MonoBehaviour
{
    [SerializeField]
    private Image image;
    [SerializeField]
    private List<Sprite> sprites;

    public  void ChangeSprite (int value)
    {
        image.sprite = sprites[value];
    }

}
