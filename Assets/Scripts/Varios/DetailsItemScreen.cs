﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DetailsItemScreen : MonoBehaviour
{
    [SerializeField]
    private Text titleProduct, descriptionProduct,marcaTExt;
    [SerializeField]
    private Product currentProduct;
    [SerializeField]
    private PrefabImageProduct imageProduct;
    [SerializeField]
    private Transform containerImages;
    [SerializeField]
    private Image backImage,itemImage;
    public Product CurrentProduct { get => currentProduct; set => currentProduct = value; }

    public void SetBackImage ()
    {
        backImage.sprite = itemImage.sprite;
        backImage.preserveAspect = true;
    }

    public void LoadCurrentProduct ()
    {
        foreach (Transform t in containerImages)
        {
            Destroy(t.gameObject);
        }
        titleProduct.text = CurrentProduct.producto.ToUpper();
        descriptionProduct.text = StringToUpper.FirstCharToUpper(CurrentProduct.descripcion);
        marcaTExt.text = currentProduct.marca;
        foreach ( var image in CurrentProduct.imagenes.imagenes)
        {
            var img = Instantiate(imageProduct, containerImages);
            img.SetupPreview(image);
        }
    }
}
