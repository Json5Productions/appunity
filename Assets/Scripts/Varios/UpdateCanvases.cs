﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateCanvases : MonoBehaviour
{
    [SerializeField]
    private RectTransform text;
    [EasyButtons.Button]
    public void UpdateCanvas ()
    {
        LayoutRebuilder.ForceRebuildLayoutImmediate(text);
    }
}
