﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscButton : MonoBehaviour
{
    float t = 0;
    bool salir = false;
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (t < 1 && salir)
            {
                Debug.Log("salir de la app");
                Application.Quit();
            }
            salir = true;
        }
        if (salir)
            t += Time.deltaTime / 1;
        if (t > 1)
        {
            salir = false;
            t = 0;
        }
    }
}
