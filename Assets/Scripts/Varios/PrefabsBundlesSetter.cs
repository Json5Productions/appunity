﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabsBundlesSetter : MonoBehaviour
{
    [SerializeField]
    private List<InfoPrefabs> infoPrefabs;

    public List<InfoPrefabs> InfoPrefabs { get => infoPrefabs; set => infoPrefabs = value; }
}

[System.Serializable]
public class InfoPrefabs
{
    public string id;
    public GameObject prefab;
}