﻿using DoozyUI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InvokeClickButton : MonoBehaviour
{
    [SerializeField]
    private Button button;
    [SerializeField]
    private UIButton buttonDozzy;

    private void Reset()
    {
        if (GetComponent<UIButton>())
            buttonDozzy = GetComponent<UIButton>();
        button = GetComponent<Button>();
    }

    public void ClickButton()
    {
        if (buttonDozzy)
            buttonDozzy.OnClick.Invoke();
        else
            button.onClick.Invoke();
    }

}
