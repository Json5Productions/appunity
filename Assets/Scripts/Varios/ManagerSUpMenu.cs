﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerSUpMenu : MonoBehaviour
{
    [SerializeField]
    private int indexTest;
    [SerializeField]
    private List<ItemMenuSup> items;
    [SerializeField]
    private AnimationControlerMenuPrincipal animationControler;
    [SerializeField]
    private NewAnimationControlerMenuPrincipal2 newAnim;

    public void ChangeMenuToItem(int id)
    {
        var item = items.Find(x => x.id == id);
        animationControler.ActiveObject(item.image);
        newAnim.HideMenu(item.label);
    }
    [EasyButtons.Button]
    private void TestPos()
    {
        ChangeMenuToItem(indexTest);
    }
}

[System.Serializable]
public class ItemMenuSup
{
    public int id;
    public GameObject label, image;
}