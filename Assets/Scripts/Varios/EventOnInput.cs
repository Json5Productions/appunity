﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventOnInput : MonoBehaviour
{
    [SerializeField]
    private KeyCode key;
    [SerializeField]
    private UnityEvent onkeyPress;
    void Update()
    {
        if (Input.GetKeyDown(key))
        {
            onkeyPress.Invoke();
        }
    }
}
