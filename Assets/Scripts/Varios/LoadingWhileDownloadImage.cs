﻿using UnityEngine;
using UnityEngine.UI;

public class LoadingWhileDownloadImage : MonoBehaviour
{
    [SerializeField]
    private GameObject image;
    public void StatusLoading(bool status)
    {
        image.SetActive(status);
    }
}
