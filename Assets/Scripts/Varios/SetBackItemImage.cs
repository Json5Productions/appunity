﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetBackItemImage : MonoBehaviour
{
    public static SetBackItemImage Instace;
    [SerializeField]
    private Image iamge;
    private void Start()
    {
        Instace = this;
    }

    public void ChangeImage (Sprite img)
    {
        iamge.sprite = img;
        iamge.preserveAspect = true;
    }

}
