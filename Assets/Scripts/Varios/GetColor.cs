﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetColor : MonoBehaviour
{
    [SerializeField]
    private Image imageRefer,thisImage;

    private void Update()
    {
        if (imageRefer.color != thisImage.color)
        {
            thisImage.color = imageRefer.color;
        }
    }

}
