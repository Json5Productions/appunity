﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ModeAppManager : MonoBehaviour
{
    [SerializeField]
    private ModeApp currentMode;
    [SerializeField]
    private UnityEvent on3dMode, onArMode;
    [SerializeField]
    private Image imagebg;

    public ModeApp CurrentMode { get => currentMode; set => currentMode = value; }

    [EasyButtons.Button]
    public void ChangeMode()
    {
        switch (CurrentMode)
        {
            case ModeApp.TresD:
                CurrentMode = ModeApp.Ar;
                break;
            case ModeApp.Ar:
                CurrentMode = ModeApp.TresD;
                break;
        }
        InvokeCurrentMode();
    }
    public void ChangeModeAR()
    {
        CurrentMode = ModeApp.Ar;
    }
    public void ChangeMode3d()
    {
        CurrentMode = ModeApp.TresD;
    }

    public void OnARMode()
    {
        onArMode.Invoke();
        imagebg.rectTransform.localScale = new Vector2(-1, 1);
    }
    public void On3DMode()
    {
        on3dMode.Invoke();
        imagebg.rectTransform.localScale = new Vector2(1, 1);
    }

    public void InvokeCurrentMode()
    {
        switch (CurrentMode)
        {
            case ModeApp.TresD:
                on3dMode.Invoke();
                imagebg.rectTransform.localScale = new Vector2(1, 1);
                break;
            case ModeApp.Ar:
                onArMode.Invoke();
                imagebg.rectTransform.localScale = new Vector2(-1, 1);
                break;
        }
    }
}
public enum ModeApp
{
    TresD,
    Ar
}
