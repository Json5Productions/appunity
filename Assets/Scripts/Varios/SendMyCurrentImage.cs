﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SendMyCurrentImage : MonoBehaviour
{
    public Image imag;
    public SetBackItemImage setBackItem;
    public void SendImage ()
    {
        setBackItem.ChangeImage(imag.sprite);
    }
}
