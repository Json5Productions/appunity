﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class ObjectInfo
{
    public Item item;
    public List<string> Colors;
    public List<Item> CompatibleOjects;
}

[System.Serializable]
public class Item
{
    public TypeObject typeObject;
    public string NameItem, IdItem;
}

public enum TypeObject
{
    bicicleta,
    parte,
    grupo,
    suspension
}