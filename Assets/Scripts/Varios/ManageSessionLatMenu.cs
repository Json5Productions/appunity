﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageSessionLatMenu : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> objectsWithSession, objectsWithoutSession;
    [SerializeField]
    private LoginPeticion loginPeticion;

    public void ManageSession ()
    {
        if (loginPeticion.isLoged)
        {
            foreach (var pet in objectsWithSession)
                pet.SetActive(true);
            foreach (var pet in objectsWithoutSession)
                pet.SetActive(false);
        }
        else
        {
            foreach (var pet in objectsWithSession)
                pet.SetActive(false);
            foreach (var pet in objectsWithoutSession)
                pet.SetActive(true);
        }
            
    }
}
