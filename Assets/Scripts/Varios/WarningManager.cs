﻿using DoozyUI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class WarningManager : MonoBehaviour
{
    public Text TextWarning,textWarningDownload;
    [SerializeField]
    private GameObject image,closebton,buttonsContainer,OkButton;

    public static WarningManager Instance;
    void Awake()
    {
        Instance = this;
    }

    public void ShowWarninDownload ()
    {
        textWarningDownload.text = "Cargando...";
        UINavigation.ShowUiElement("warningDownload", "StartScreens", false);
        UINavigation.ShowUiElement("Menudistorsion", "StartScreens", false);
        buttonsContainer.SetActive(false);
    }

    public void SetTextWarningDownlaod (string text)
    {
        buttonsContainer.SetActive(true);
        textWarningDownload.text = $"Para continuar, debes descargar {text}MB ¿quieres continuar?";
    }

    public void ShowWarning (string Message)
    {
        image.SetActive(false);
        closebton.SetActive(true);
        OkButton.SetActive(false);
        TextWarning.text = Message;
        UINavigation.ShowUiElement("warningResgiter", "StartScreens", false);
        UINavigation.ShowUiElement("Menudistorsion", "StartScreens", false);
    }
    public void ShowWarning (string Message,UnityAction action)
    {
        image.SetActive(false);
        closebton.SetActive(false);
        TextWarning.text = Message;
        UINavigation.ShowUiElement("warningResgiter", "StartScreens", false);
        UINavigation.ShowUiElement("Menudistorsion", "StartScreens", false);
        OkButton.SetActive(true);
        OkButton.GetComponent<Button>().onClick.AddListener(action);
    }
    public void ShowLoading ()
    {
        closebton.SetActive(false);
        TextWarning.text = "Cargando...";
        image.SetActive(true);
        UINavigation.ShowUiElement("warningResgiter", "StartScreens", false);
        UINavigation.ShowUiElement("Menudistorsion", "StartScreens", false);
    }

    public void HideLoading ()
    {
        UINavigation.HideUiElement("Menudistorsion", "StartScreens", false);
        UINavigation.HideUiElement("warningResgiter", "StartScreens", false);
    }

}
