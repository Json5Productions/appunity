﻿using DoozyUI;
using UnityEngine;
using UnityEngine.UI;

public class PrefabItemStorepreview : MonoBehaviour
{
    [SerializeField]
    private Image image;
    [SerializeField]
    private UIButton button;
    [SerializeField]
    private Outline outline;
    private Product product;

    public Image Image { get => image; set => image = value; }

    public void ManageOutline (bool enable)
    {
        outline.enabled = enable;
    }

    private void Start()
    {  
        button.OnClick.AddListener(OnClick);
    }

    public void OnClick()
    {
        ResponseSpecificProduct current = new ResponseSpecificProduct();
        current.row = product;
        ScreenStoreItemSelected.Instace.UpdateCurrentProduct(current, image.sprite);
    }
    public void SetupPreview(Product item, string urlImage)
    {
        product = item;
        var downloader = gameObject.AddComponent<DownloaderImages>();
        downloader.RawImage = Image;
        if (!string.IsNullOrEmpty(urlImage))
            downloader.DownloadImage(urlImage);
        else
            downloader.ActiveLoadingByStatus(true);
    }
    public void SetupPreview(Product item)
    {
        product = item;
        var downloader = gameObject.AddComponent<DownloaderImages>();
        downloader.RawImage = Image;
        downloader.ActiveLoadingByStatus(false);
    }
}
