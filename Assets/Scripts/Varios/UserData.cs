﻿using System;
using System.Collections.Generic;

[Serializable]
public class UserData 
{
    public string usuario ;
    public string documento_tipo ;
    public string documento ;
    public string clave ;
    public string temporal ;
    public int activo ;
    public string nombre ;
    public string perfil ;
    public string email ;
    public string telefono ;
    public string movil ;
    public string direccion ;
    public string GUID ;
    public string authcode ;
    public object restaurado ;
    public string creo ;
    public string creado ;
    public string actualizo ;
    public string actualizado ;
}
[Serializable]
public class UserLoginResponse
{
    public bool error ;
    public List<Favorite> favorites;
    public string user ;
    public UserData row ;
}
[Serializable]

public class Favorite
{
    public int id;
    public string producto;
}

public class RegisterResponse
{
    public bool error ;
    public string status ;
    public string user ;
}