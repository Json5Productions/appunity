﻿using DoozyUI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeOrientation : MonoBehaviour
{

    public void ChangeOrientationToLandscape ()
    {
        Screen.orientation = ScreenOrientation.LandscapeLeft;
    }
    public void ChangeOrientationToPortraid ()
    {
        Screen.orientation = ScreenOrientation.Portrait;
    }

}
