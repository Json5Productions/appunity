﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PrefabSubCategorie : MonoBehaviour
{
    [SerializeField]
    private Image image;
    [SerializeField]
    private Text titleCat;
    private int subCatIndex;

    public void SetupItem (string title, string url, int subCat)
    {
        this.subCatIndex = subCat;
        titleCat.text = title;
        var downloader = gameObject.AddComponent<DownloaderImages>();
        downloader.RawImage = image;
        if (!string.IsNullOrEmpty(url))
            downloader.DownloadImage(url);
        else
            downloader.ActiveLoadingByStatus(true);
    }

    public void ClickButton ()
    {
        ScreenStoreItemSelected.Instace.SetNewCategorie(subCatIndex);
        ScreenStoreItemSelected.Instace.GetProductsInCurrentPage();
    }

}
