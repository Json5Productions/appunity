﻿using DoozyUI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PrefabItemList : MonoBehaviour
{
    [SerializeField]
    private Image image;
    [SerializeField]
    private Text nameItem,marcaItem,PriceItem;
    [SerializeField]
    private Product product;
    [SerializeField]
    private UIButton button;

    private void Start()
    {
        button.OnClick.AddListener(ClickFavButton);
    }

    public void SetupItem(Product product)
    {
        this.product = product;
        nameItem.text = product.producto;
        if (!string.IsNullOrEmpty(product.marca))
        {
            marcaItem.text = $"<color=red><b>Marca: </b></color>{product.marca}";
        }
        else
            marcaItem.gameObject.SetActive(false);

        var pricingstr = string.Format("{0:c0}", Convert.ToDecimal(product.valor));
        pricingstr = pricingstr.Replace(pricingstr[0], '$');
        pricingstr = pricingstr.Insert(1, " ");
        PriceItem.text = $"<color=red><b>Precio: </b></color>{pricingstr}";
        var downloader = gameObject.AddComponent<DownloaderImages>();
        downloader.RawImage = image;
        if (product != null && product.imagenes.imagenes.Count>0)
        {
            if (!string.IsNullOrEmpty(product.imagenes.imagenes[0]))
                downloader.DownloadImage(product.imagenes.imagenes[0]);
            else
                downloader.ActiveLoadingByStatus(true);
        }else
        {
            downloader.ActiveLoadingByStatus(true);
        }
    }

    public void ClickButton ()
    {
        ResponseSpecificProduct current = new ResponseSpecificProduct();
        current.row = product;
        ScreenStoreItemSelected.Instace.UpdateCurrentProduct(current, image.sprite);
    }

    public void ClickFavButton()
    {
        StartCoroutine(AddToFavs());
    }

    private IEnumerator AddToFavs()
    {
        if (LoginPeticion.Instace.isLoged)
        {
            WWWForm form = new WWWForm();
            form.AddField("user", LoginPeticion.Instace.UserData.user);
            form.AddField("product", product.id);

            UnityWebRequest www = UnityWebRequest.Post("https://bikebi.com.co/restapi/v1/agregarFavorito", form);
            www.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            www.SetRequestHeader("Authorization", "3d524a53c110e4c22463b10ed32cef9d");
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.downloadHandler.text);
                WarningManager.Instance.ShowWarning("Ups, algo ha salido mal, por favor verifica tus datos.");
            }
            else
            {
                ManagerUiWayScript.Instance.Category = "StartScreens";
                ManagerUiWayScript.Instance.ShowScreen("FavNot");
                StartCoroutine(TimeToHide());
            }
        }
        else
        {
            WarningManager.Instance.ShowWarning("Ups, primero debes iniciar sesion para poder agregar favoritos.");
        }
    }
    private IEnumerator TimeToHide()
    {
        yield return new WaitForSeconds(3);
        ManagerUiWayScript.Instance.HideScreen("FavNot");
    }
}
