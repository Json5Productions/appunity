﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTagComponent : MonoBehaviour
{
    [SerializeField]
    private string tagTarget;

    [SerializeField]
    private Transform target;

    private void Update()
    {
        if (target != null)
        {
            transform.position = target.position;
        }
    }

    public void SetTarget ()
    {
        target = GameObject.FindGameObjectWithTag(tagTarget).transform;
    }

}
