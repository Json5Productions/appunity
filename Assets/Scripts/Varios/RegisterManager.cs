﻿using DoozyUI;
using Newtonsoft.Json;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class RegisterManager : MonoBehaviour
{
    [SerializeField]
    private Toggle toggle;
    [SerializeField]
    private Text emailText;
    [SerializeField]
    private LoginPeticion loginPeticion;
    public void CheckConditionsToResgiter()
    {
        if (!toggle.isOn)
        {
            WarningManager.Instance.ShowWarning("Debes aceptar los terminos y condiciones para continuar.");
            return;
        }
        loginPeticion.SendRegisterPetition();
    }

}