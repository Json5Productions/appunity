﻿using UnityEngine;
using UnityEngine.UI;

public class UIZoomImage : MonoBehaviour
{
    private Vector3 initialScale, min, max;
    [SerializeField]
    private float maxZoom = 10f;
    private void Awake()
    {
        initialScale = transform.localScale;
    }
    private void OnEnable()
    {
        transform.localScale = initialScale;
    }
    private void Start()
    {
        min = initialScale;
        max = initialScale * maxZoom;
    }
    private void Update()
    {
        
    }
    public void OnValueChanged(float value)
    {
        transform.localScale = Vector3.Lerp(min,max,value);
    }


  
}