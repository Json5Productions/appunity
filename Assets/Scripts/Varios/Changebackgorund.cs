﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Changebackgorund : MonoBehaviour
{
    [SerializeField]
    private List<Material> materials;
    [SerializeField]
    private List<Texture> textures;
    private int index;

    public void ChangeBackground ()
    {
        if (index < textures.Count-1)
        {
            index++;
        }
        else
        {
            index = 0;
        }
        foreach (var mat in materials)
        {
            mat.mainTexture = textures[index];
        }
    }

}
