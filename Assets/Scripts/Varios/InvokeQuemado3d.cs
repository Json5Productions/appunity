﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InvokeQuemado3d : MonoBehaviour
{
    [SerializeField]
    private UnityEvent onQuemado3d, onBundleObjects;

    public void InvokeQuemado ()
    {
        onQuemado3d.Invoke();
    }
    public void InvokeBundle ()
    {
        onBundleObjects.Invoke();
    }

}
