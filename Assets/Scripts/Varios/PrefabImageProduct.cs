﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PrefabImageProduct : MonoBehaviour
{
    [SerializeField]
    private Image image;
    public void SetupPreview (string urlImage)
    {
        var downloader = gameObject.AddComponent<DownloaderImages>();
        downloader.RawImage = image;
        if (!string.IsNullOrEmpty(urlImage))
            downloader.DownloadImage(urlImage);
        else
            downloader.ActiveLoadingByStatus(true);
    }

    public void OnClick()
    {
        SetBackItemImage.Instace.ChangeImage(image.sprite);
    }
}
