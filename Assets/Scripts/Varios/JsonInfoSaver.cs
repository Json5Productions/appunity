﻿using Newtonsoft.Json;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class JsonInfoSaver : MonoBehaviour
{
    [SerializeField]
    private ObjectInfo info;

    public ObjectInfo Info { get => info; set => info = value; }

    [EasyButtons.Button]
    public void SaveJsonInfo ()
    {
#if UNITY_EDITOR

        string file = Application.dataPath + "/Resources/JsonInfo/" + Info.item.IdItem + ".txt";
        using (var writer = new StreamWriter(File.Create(file)))
        {
            string contend = JsonConvert.SerializeObject(Info);
            writer.Write(contend);
            writer.Close();
        }
       

        //Re-import the file to update the reference in the editor
        AssetDatabase.ImportAsset(file);
#endif
    }


    [EasyButtons.Button]
    public void LoadJson ()
    {
        var text = Resources.Load("JsonInfo/" + Info.item.IdItem) as TextAsset;
        info = JsonConvert.DeserializeObject<ObjectInfo>(text.text);
    }

}
