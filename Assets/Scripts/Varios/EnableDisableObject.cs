﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableDisableObject : MonoBehaviour
{
    [SerializeField]
    private GameObject parentGrupos, PriceGameobject;

    public void managePriceObject ()
    {
        if (parentGrupos.activeSelf)
        {
            PriceGameobject.SetActive(!PriceGameobject.activeSelf);
        }
    }

}
