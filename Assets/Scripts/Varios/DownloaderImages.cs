﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class DownloaderImages : MonoBehaviour
{
    [SerializeField]
    private Image rawImage;
    [SerializeField]
    private string url;

    public Action onSetImage;

    public Image RawImage { get => rawImage; set => rawImage = value; }

    public void DownloadImage(string url)
    {
        if (ImagesSavecInRam.Instance.imagesStoreds.Exists(x => x.idimage == url))
        {
            var img = ImagesSavecInRam.Instance.imagesStoreds.Find(x => x.idimage == url);
            if (img != null)
            {
                RawImage.sprite = img.sprite;
                rawImage.preserveAspect = true;
                ActiveLoadingByStatus(false);
                onSetImage?.Invoke();
            }else
                StartCoroutine(PetitionDownload(url));
        }
        else
            StartCoroutine(PetitionDownload(url));
    }
    public void DownloadSprite(string url, Texture2D sprite)
    {
        StartCoroutine(PetitionDownloadSprite(url, sprite));
    }
    public void ActiveLoadingByStatus(bool status)
    {
        rawImage.transform.GetChild(0).gameObject.SetActive(status);
    }
    private IEnumerator PetitionDownload(string url)
    {
        UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(url);
        uwr.useHttpContinue = false;
        using (uwr)
        {
            ActiveLoadingByStatus(true);
            yield return uwr.SendWebRequest();
            if (uwr.isNetworkError || uwr.isHttpError)
            {
                Debug.Log(uwr.error + " " + url);
            }
            else
            {
                var tex = DownloadHandlerTexture.GetContent(uwr);
                var sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
                ImagesSavecInRam.Instance.imagesStoreds.Add(new ImagesStored(url,sprite));
                RawImage.sprite = sprite;
                rawImage.preserveAspect = true;
                ActiveLoadingByStatus(false);
                onSetImage?.Invoke();
                Destroy(this);
            }
        }
    }
    private IEnumerator PetitionDownloadSprite(string url, Texture2D sprite)
    {
        UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(url);
        uwr.useHttpContinue = false;
        using (uwr)
        {
            ActiveLoadingByStatus(true);
            yield return uwr.SendWebRequest();
            if (uwr.isNetworkError || uwr.isHttpError)
            {
                Debug.Log(uwr.error);
            }
            else
            {
                var tex = DownloadHandlerTexture.GetContent(uwr);
                sprite = tex;
                var sprites = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
                ImagesSavecInRam.Instance.imagesStoreds.Add(new ImagesStored(url, sprites));
                Destroy(this);
            }
        }
    }


    public void DownloadTexture(string url, Material mat)
    {
        StartCoroutine(PetitionDownloadTexture(url, mat));
    }
    private IEnumerator PetitionDownloadTexture(string url, Material mat)
    {
        using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(url))
        {
            yield return uwr.SendWebRequest();
            if (uwr.isNetworkError || uwr.isHttpError)
            {
                Debug.Log(uwr.error);
            }
            else
            {
                var tex = DownloadHandlerTexture.GetContent(uwr);
                mat.mainTexture = tex;
                Destroy(this);
            }
        }
    }
}
