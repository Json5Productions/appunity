﻿using UnityEngine;
using DoozyUI;
using UnityEngine.Events;
using System.Collections;

public class ShowItemByQE : MonoBehaviour
{
    [SerializeField]
    private GameObject canvas;
    [SerializeField]
    private ScreenStoreItemSelected screenStoreItem;
    [SerializeField]
    private UnityEvent onShowQRItem;
    private void Start()
    {
        DynamicLinkListener.Instance.OnItemRequested += InvokeItemByQR;
    }

    [EasyButtons.Button]
    public void TestQr ()
    {
        InvokeItemByQR(9);
    }

    IEnumerator waitForStart (int idItem)
    {
        yield return new WaitUntil(()=>canvas.activeSelf);
        screenStoreItem.GetSpecifidProduct(idItem);
        onShowQRItem.Invoke();
        UINavigation.ShowUiElementAndHideAllTheOthers("ItemsStoreScreen", "StartScreens", false);
        UINavigation.ShowUiElement("Inicio", "StartScreens", false);
        UINavigation.ShowUiElement("menuInferior", "StartScreens", false);
    }

    public void InvokeItemByQR (int idItem)
    {
        StartCoroutine(waitForStart(idItem));
    }
}
