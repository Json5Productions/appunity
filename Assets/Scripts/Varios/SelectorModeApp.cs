﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectorModeApp : MonoBehaviour
{
    [SerializeField]
    private ModeAppManager modeApp;
    [SerializeField]
    private InvokeClickButton button;
    [SerializeField]
    private ModeApp mode;
    public void SetToAR ()
    {
        mode = ModeApp.Ar;
    }

    public void SetTo3D ()
    {
        mode = ModeApp.TresD;
    }

    public void SetMode ()
    {
        switch (mode)
        {
            case ModeApp.TresD:
                button.GetComponent<ModeAppManager>().On3DMode();
                break;
            case ModeApp.Ar:
                button.GetComponent<ModeAppManager>().OnARMode();
                break;
        }
    }
}