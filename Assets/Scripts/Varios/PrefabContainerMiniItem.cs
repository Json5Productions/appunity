﻿using DoozyUI;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PrefabContainerMiniItem : MonoBehaviour
{
    [SerializeField]
    private Image image;
    [SerializeField]
    private Text titleItem, typeItem, MarcaItem, Valueitem;
    [SerializeField]
    private UIButton button, FavButton, ShowMoreButton, StoreButton;

    private Product productData;

    public Product ProductData { get => productData; set => productData = value; }

    private void Start()
    {
        FavButton.OnClick.AddListener(ClickFavButton);
       // ShowMoreButton.OnClick.AddListener(OnCLick);
        StoreButton.OnClick.AddListener(ClickStoreButton);
    }

    public void SetupItem(string title, string types, string marca, string value, string urlImage)
    {
        var pricingstr = string.Format("{0:c0}", Convert.ToDecimal(value));
        pricingstr = pricingstr.Replace(pricingstr[0], '$');
        pricingstr =  pricingstr.Insert(1, " ");
        titleItem.text = $"<b>{title.ToUpper()}</b>";
        typeItem.text = $"<color=red>Categoria: </color>{ StringToUpper.FirstCharToUpper(types)}";
        MarcaItem.text = $"<color=red>Marca: </color>{ StringToUpper.FirstCharToUpper(marca)}";
        Valueitem.text = $"<color=red>Precio: </color>{pricingstr}";
        button.OnClick.AddListener(OnCLick);
        image.sprite = null;
        var downloader = gameObject.AddComponent<DownloaderImages>();
        downloader.RawImage = image;
        if (!string.IsNullOrEmpty(urlImage))
            downloader.DownloadImage(urlImage);
        else
            downloader.ActiveLoadingByStatus(true);
    }

    public void ClickFavButton()
    {
        Debug.Log("persionado");
        StartCoroutine(AddToFavs());
    }

    private IEnumerator AddToFavs()
    {
        if (LoginPeticion.Instace.isLoged)
        {
            WWWForm form = new WWWForm();
            form.AddField("user", LoginPeticion.Instace.UserData.user);
            form.AddField("product", productData.id);

            UnityWebRequest www = UnityWebRequest.Post("https://bikebi.com.co/restapi/v1/agregarFavorito", form);
            www.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            www.SetRequestHeader("Authorization", "3d524a53c110e4c22463b10ed32cef9d");
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.downloadHandler.text);
                WarningManager.Instance.ShowWarning("Ups, algo ha salido mal, por favor verifica tus datos.");
            }
            else
            {
                ManagerUiWayScript.Instance.Category = "StartScreens";
                ManagerUiWayScript.Instance.ShowScreen("FavNot");
                StartCoroutine(TimeToHide());
            }
        }
        else
        {
            WarningManager.Instance.ShowWarning("Ups, primero debes iniciar sesion para poder agregar favoritos.");
        }
    }
    private IEnumerator TimeToHide()
    {
        yield return new WaitForSeconds(3);
        ManagerUiWayScript.Instance.HideScreen("FavNot");

    }

    public void ClickStoreButton()
    {
        Application.OpenURL("https://bikebi.com.co/producto?id=" + ProductData.id);
    }

    public void OnCLick()
    {
        ScreenStoreItemSelected.Instace.GetSpecifidProduct(ProductData.id);
        AnimationControlerMenuPrincipal.Instance.ShowMenu(AnimationControlerMenuPrincipal.Instance.Images[0]);
    }
}

public class StringToUpper
{
    public static string FirstCharToUpper(string input)
    {
        var str = "";
        foreach (var ch in input)
        {
            string s = "";
            s += ch;
            if (ch == input.First())
            {
                str += s.ToUpper();
            }
            else
            {
                str += s.ToLower();
            }
        }
        return str;
    }
}