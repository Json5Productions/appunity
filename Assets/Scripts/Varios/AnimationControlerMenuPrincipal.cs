﻿using System.Collections.Generic;
using UnityEngine;

public class AnimationControlerMenuPrincipal : MonoBehaviour
{
    [SerializeField]
    private Animator animator;
    [SerializeField]
    private string state;
    private bool open;
    [SerializeField]
    private List<GameObject> images;
    public static AnimationControlerMenuPrincipal Instance;

    public List<GameObject> Images { get => images; set => images = value; }

    private void Awake()
    {
        if (AnimationControlerMenuPrincipal.Instance == null)
            AnimationControlerMenuPrincipal.Instance = this;
        else
            Destroy(this);
    }
    public void ShowMenu(GameObject image)
    {
        if (animator)
            animator.SetBool(state, true);
        foreach (var img in Images)
        {
            if (img == image)
                img.SetActive(true);
            else
                img.SetActive(false);
        }
    }
    public void ActiveObject (GameObject image)
    {
        foreach (var img in Images)
        {
            if (img == image)
                img.SetActive(true);
            else
                img.SetActive(false);
        }
    }

    public void HideMenu(GameObject image)
    {
        if (animator)
            animator.SetBool(state, false);
        foreach (var img in Images)
        {
            if (img == image)
                img.SetActive(true);
            else
                img.SetActive(false);
        }
    }

}
