﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class ChangeScrollPosicition : MonoBehaviour
{
    [SerializeField]
    private ScrollRect scroll;
    [SerializeField]
    private int index;
    [SerializeField]
    private List<float> poscitions;
    [SerializeField]
    private List<Animator> animators;
    [SerializeField]
    private List<string> categoriesList;
    [SerializeField]
    private AnimationController animationController;
    [SerializeField]
    private GetProducts getProducts;
    [SerializeField]
    private UnityEventInt onSlideEnd;
    private float timer;
    private bool move;
    public void ChangePocition(float pos)
    {
        scroll.horizontalNormalizedPosition = pos;
    }

    [EasyButtons.Button]
    public void SwipeLeft()
    {
        ChangeIndex(-1);
    }
    [EasyButtons.Button]
    public void SwipeRigth()
    {
        ChangeIndex(1);
    }

    public void ChangeIndex(int sentido)
    {
        var temp = index;
        if (sentido > 0 && index < poscitions.Count - 1)
            index++;
        else if (sentido < 0 && index > 0)
            index--;
        if (temp != index)
        {
            move = true;
            timer = 0;
        }
        getProducts.GetProductsPetitionBicisMini(categoriesList[index]);
    }

    private void Update()
    {
        //Debug.Log(scroll.horizontalNormalizedPosition);
        if (move)
        {
            timer += Time.deltaTime * 2;
            if (timer < 1)
            {
                animationController.ChangeParameterState(animators[index]);
                scroll.horizontalNormalizedPosition = Mathf.Lerp(scroll.horizontalNormalizedPosition, poscitions[index], timer);
            }
            else
            {
                onSlideEnd.Invoke(index);
                move = false;
                timer = 0;
            }
        }
    }

}
