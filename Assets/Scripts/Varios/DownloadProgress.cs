﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DownloadProgress : MonoBehaviour
{
    [SerializeField]
    private Image image;
    [SerializeField]
    private Text text,textDescript;

    public Text TextDescript { get => textDescript; set => textDescript = value; }

    void Update()
    {
        image.fillAmount = AssetBundleManager.Instance.DownloadProgress;
        if (AssetBundleManager.Instance.DownloadProgress > 0)
        {
            text.text = (AssetBundleManager.Instance.DownloadProgress * 100).ToString("0") + "%";
        }
    }
}
