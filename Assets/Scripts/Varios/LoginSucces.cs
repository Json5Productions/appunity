﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoginSucces : MonoBehaviour
{
    [SerializeField]
    private List<Text> nameTexts;
    [SerializeField]
    private Text cc, user, tel, dir;
    [SerializeField]
    private LoginPeticion peticionData;

    public void SetuserDataToUi ()
    {
        foreach(var t in nameTexts)
        {
            t.text = peticionData.UserData.row.nombre;
        }
        cc.text = $"<color=red>{peticionData.UserData.row.documento_tipo}: </color>{peticionData.UserData.row.documento}";
        user.text = $"<color=red>Usuario: </color>{peticionData.UserData.row.usuario}";
        tel.text = $"<color=red>Telefono: </color>{peticionData.UserData.row.telefono}";
        dir.text = $"<color=red>Direccion: </color>{peticionData.UserData.row.direccion}";
    }

}
