﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
public class ChangeColorToPArts : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> parts;
    [SerializeField]
    private Color currentColor = Color.white;
    [SerializeField]
    private AssetBundleInstantiatorExample assetBundle;

    private int index;

    public void SetColorToRed ()
    {
        ChangeColor(Color.red);
    }
    public void SetColorToYellow ()
    {
        ChangeColor(Color.yellow);
    }
    public void SetColorToWhite ()
    {
        ChangeColor(Color.white);
    }

    public void ChangeColor ()
    {
        var color = assetBundle.CurrentObjectInfo.Colors[index];
        ColorUtility.TryParseHtmlString("#"+color,out currentColor);
        ChangeColor(currentColor);
        if (index >= assetBundle.CurrentObjectInfo.Colors.Count - 1)
            index = 0;
        else
            index++;
    }

    public void ChangeColor (Color color)
    {
        parts = GameObject.FindGameObjectsWithTag("Color").ToList();
        foreach (var obj in parts)
        {
            foreach(var mat in obj.GetComponent<MeshRenderer>().materials)
            {
                mat.SetColor("_BaseColor", color);
            }
        }
    }

}
