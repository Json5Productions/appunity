﻿using DoozyUI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerUiWayScript : MonoBehaviour
{
    private string category;
    public static ManagerUiWayScript Instance;
    public string Category { get => category; set => category = value; }

    private void Start()
    {
        Instance = this;
    }

    public void ShowScreen(string NameScreen)
    {
        UINavigation.ShowUiElement(NameScreen, Category, false);
    }
    public void HideScreen (string NameScreen)
    {
        UINavigation.HideUiElement(NameScreen, Category, false);
    }
}
