﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    [SerializeField]
    private List<Animator> animators = new List<Animator>();
    [SerializeField]
    private string nameParameter;
    [SerializeField]
    private bool status = true;

    public bool Status { get => status; set => status = value; }

    public void ChangeSpecific(int index)
    {
        animators[index].SetBool(nameParameter, status);
    }

    public void ChangeParameterState(Animator animator)
    {
        foreach (var anim in animators)
        {
            if (animator == anim)
                anim.SetBool(nameParameter, status);
            else
                anim.SetBool(nameParameter, !status);
        }
    }

}
