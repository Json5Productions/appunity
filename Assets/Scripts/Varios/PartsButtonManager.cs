﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PartsButtonManager : MonoBehaviour
{
    [SerializeField]
    private AssetBundleInstantiatorExample assetBundle;

    [SerializeField]
    private UnityEvent onBikeType, onPartsType;

    public void ButtonClick ()
    {
        if (assetBundle.CurrentObjectInfo.item.typeObject == TypeObject.bicicleta)
        {
            onBikeType.Invoke();
        }
        else
            onPartsType.Invoke();
    }

}
