﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class backgorundMenumovement : MonoBehaviour
{
    [SerializeField]
    private RawImage image;
    [SerializeField]
    private float sensibility = 5f;
    private float nextPos, timer;
    private bool move = false;

    public void MoveImg(float position)
    {
        nextPos = position;
        move = true;
        timer = 0;
    }
    private void Update()
    {
        if (move)
        {
            timer += Time.deltaTime / sensibility;
            if (timer < 1)
            {
                var rect = image.uvRect;
                rect.x = Mathf.Lerp(image.uvRect.x, nextPos, timer);
                image.uvRect = rect;
            }
            else
            {
                move = false;
            }
        }
    }
}
