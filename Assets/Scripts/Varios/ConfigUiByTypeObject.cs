﻿using DoozyUI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfigUiByTypeObject : MonoBehaviour
{
    [SerializeField]
    private GameObject gruposButton, partsButton, colorsButton, suspensionButton,groupUI,PartsUI,colorsUI,suspensionUI;

    [SerializeField]
    private AssetBundleInstantiatorExample assetBundle;

    private bool active =false;

    public void ConfigInterface()
    {
        active = false;
        groupUI.SetActive(false);
        PartsUI.SetActive(false);
        colorsUI.SetActive(false);
        suspensionUI.SetActive(false);
        gruposButton.SetActive(false);
        partsButton.SetActive(false);
        colorsButton.SetActive(false);
        suspensionButton.SetActive(false);
        if (assetBundle.CurrentObjectInfo.item.typeObject == TypeObject.parte)
        {
            gruposButton.SetActive(false);
            partsButton.SetActive(false);
            colorsButton.SetActive(false);
            suspensionButton.SetActive(false);
        }
        else
        {
            if (assetBundle.CurrentObjectInfo.Colors.Count > 0)
            {
                colorsButton.GetComponent<UIButton>().SendButtonClick();
                colorsButton.SetActive(true);
                active = true;
                colorsUI.SetActive(true);
            }
            if (assetBundle.CurrentObjectInfo.CompatibleOjects.Exists(x => x.typeObject == TypeObject.grupo))
            {
                gruposButton.SetActive(true);
                if (!active)
                {
                    gruposButton.GetComponent<UIButton>().SendButtonClick();
                    active = true;
                }
            }
                
            if (assetBundle.CurrentObjectInfo.CompatibleOjects.Exists(x => x.typeObject == TypeObject.suspension))
            {
                suspensionButton.SetActive(true);
                if (!active)
                {
                    active = true;
                    suspensionButton.GetComponent<UIButton>().SendButtonClick();
                }
            }
            if (assetBundle.CurrentObjectInfo.item.typeObject == TypeObject.bicicleta || assetBundle.CurrentObjectInfo.item.typeObject == TypeObject.grupo)
            {
                partsButton.SetActive(true);
                if (!active)
                {
                    active = true;
                    partsButton.GetComponent<UIButton>().SendButtonClick();
                }
            }

        }
    }


}
