﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuInfCHangeManager : MonoBehaviour
{
    [SerializeField]
    private int indexTest;
    [SerializeField]
    private List<MenuInfItem> itemsMenu;
    [SerializeField]
    backgorundMenumovement movement;
    [SerializeField]
    private AnimationController animationController;
    public void ChangeMenuToItem(int id) {
        var item = itemsMenu.Find(x => x.id == id);
        movement.MoveImg(item.pos);
        animationController.ChangeParameterState(item.anim);
    }
    [EasyButtons.Button]
    private void TestPos ()
    {
        ChangeMenuToItem(indexTest);
    }
}

[System.Serializable]
public class MenuInfItem
{
    public int id;
    public float pos;
    public Animator anim;
}

