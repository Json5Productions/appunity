﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventIfParentHasChilds : MonoBehaviour
{
    public Transform parent;
    public UnityEvent doActionYes,doActionNo;

    public void CheckChilds()
    {
        if (parent.childCount > 0)
            doActionYes.Invoke();
        else
            doActionNo.Invoke();
    }

}
