﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ChangePartsBasic : MonoBehaviour
{
    [SerializeField]
    private GameObject bicicleta;
    [SerializeField]
    private List<PartsToEnable> list;
    [SerializeField]
    private List<GameObject> allParts;
    [SerializeField]
    private int index;
    [SerializeField]
    private GameObject nextBton, lastBton;
    [SerializeField]
    private Text nameText, TypeIndex;
    [SerializeField]
    private Controller controller;
    [SerializeField]
    private string subtitle = "Grupo: ";
    [SerializeField]
    private UnityEvent onPartChanged;
    [SerializeField]
    private AssetBundleInstantiatorExample assetBundle;
    public GameObject Bicicleta { get => bicicleta; set => bicicleta = value; }
    public List<GameObject> AllParts { get => allParts; set => allParts = value; }
    public List<PartsToEnable> List { get => list; set => list = value; }

    private void Start()
    {
    }

    public void ClearAllData()
    {
        Bicicleta = null;
        AllParts.Clear();
        List.Clear();
        index = 0;
    }

    public void NextButton()
    {
        index++;
        ManajeObjects();
        OnChangeParts();
    }

    public void ResetIndex()
    {
        index = 0;
        ManajeObjects();
        OnChangeParts();
    }
    public void LastButton()
    {
        index--;
        ManajeObjects();
        OnChangeParts();
    }

    public void DisableAllParts()
    {
        foreach (var obj in AllParts)
        {
            obj.SetActive(false);
        }
    }

    public void ManajeObjects()
    {
        DisableAllParts();
        if (List.Count > 0)
        {
            foreach (var obj in List[index].listObjects)
            {
                obj.SetActive(true);
                nameText.text = List[index].NameParts;
                TypeIndex.text = subtitle + (index + 1);
            }

        }
      
    }

    public void OnChangeParts()
    {
        if (index == 0)
            lastBton.SetActive(false);

        if (List.Count > 1)
        {
            if (index == 0)
            {
                lastBton.SetActive(false);
                nextBton.SetActive(true);
            }
            else if (index == List.Count - 1)
            {
                lastBton.SetActive(true);
                nextBton.SetActive(false);
            }
            else
            {
                lastBton.SetActive(true);
                nextBton.SetActive(true);
            }
        }
        else
        {
            lastBton.SetActive(false);
            nextBton.SetActive(false);
        }
        controller.ResetPos = true;
        if (assetBundle)
        {
            var item = assetBundle.CurrentObjectInfo.CompatibleOjects.Find(x => x.NameItem == nameText.text);
            //if (item != null)
                //assetBundle.RefreshPrice(item.IdItem);
        }

    }
}

[System.Serializable]
public class PartsToEnable
{
    public string NameParts;
    public List<GameObject> listObjects = new List<GameObject>();
}