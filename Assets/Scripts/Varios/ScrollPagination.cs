﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ScrollPagination : MonoBehaviour
{
    [SerializeField]
    private bool showPos,animate;
    [SerializeField]
    private float timeAnimation;
    [SerializeField]
    private ScrollRect scroll;
    [SerializeField]
    private int index;
    [SerializeField]
    private List<float> poscitions;
    [SerializeField]
    private UnityEventInt onSlideEnd;
    private float timer;
    private bool move,izq,der=true;
    public void ChangePocition(float pos)
    {
        scroll.horizontalNormalizedPosition = pos;
    }

    private void Start()
    {
        if (animate)
            StartCoroutine(Animation());
    }

    private IEnumerator Animation ()
    {
        yield return new WaitForSeconds(timeAnimation);
        if (der)
            SwipeRigth();
        else
            SwipeLeft();

        if (index ==0)
        {
            der = true;
            izq = false;
        }
        if (index == poscitions.Count - 1)
        {
            der = false;
            izq = true;
        }
        StartCoroutine(Animation());
    }

    [EasyButtons.Button]
    public void SwipeLeft()
    {
        ChangeIndex(-1);
    }
    [EasyButtons.Button]
    public void SwipeRigth()
    {
        ChangeIndex(1);
    }

    public void ChangeIndex(int sentido)
    {
        var temp = index;
        if (sentido > 0 && index < poscitions.Count - 1)
            index++;
        else if (sentido < 0 && index > 0)
            index--;
        if (temp != index)
        {
            move = true;
            timer = 0;
        }
    }

    private void Update()
    {
        if (showPos)
            Debug.Log(scroll.horizontalNormalizedPosition);
        if (move)
        {
            timer += Time.deltaTime * 2;
            if (timer < 1)
            {
                scroll.horizontalNormalizedPosition = Mathf.Lerp(scroll.horizontalNormalizedPosition, poscitions[index], timer);
            }
            else
            {
                onSlideEnd.Invoke(index);
                move = false;
                timer = 0;
            }
        }
    }
}
