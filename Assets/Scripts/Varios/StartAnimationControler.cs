﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class StartAnimationControler : MonoBehaviour
{
    [SerializeField]
    private UnityEvent starApp;
    public void StartApp ()
    {
        starApp.Invoke();
    }
}
