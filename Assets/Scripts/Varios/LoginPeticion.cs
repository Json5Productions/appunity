﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Newtonsoft.Json;
using UnityEngine.Events;
using System;
using RestSharp.Contrib;

public class LoginPeticion : MonoBehaviour
{
    [SerializeField]
    private InputField email,pass,emailregister;
    [SerializeField]
    private UserLoginResponse userData;
    [SerializeField]
    private UnityEvent OnLogin;

    public bool isLoged;
    public static LoginPeticion Instace;

    public UserLoginResponse UserData { get => userData; set => userData = value; }

    private void Start()
    {
        Instace = this;
        AutoLogin();
    }

    public void SendLoginPetition()
    {
        StartCoroutine(Upload());
    }

    IEnumerator Upload() { 

        WWWForm form = new WWWForm();
        form.AddField("user", email.text);
        form.AddField("password", pass.text);

        UnityWebRequest www = UnityWebRequest.Post("https://bikebi.com.co/restapi/v1/usuario", form);
        www.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        www.SetRequestHeader("Authorization", "3d524a53c110e4c22463b10ed32cef9d");
        WarningManager.Instance.ShowLoading();
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            WarningManager.Instance.ShowWarning("Ups, algo ha salido mal, por favor verifica tus datos.");
        }
        else
        {
            try
            {
                UserData = JsonConvert.DeserializeObject<UserLoginResponse>(www.downloadHandler.text);
                PlayerPrefs.SetString("user", email.text);
                PlayerPrefs.SetString("pass", pass.text);
                WarningManager.Instance.HideLoading();
                OnLogin.Invoke();
                isLoged = true;
            }
            catch (Exception e)
            {
                WarningManager.Instance.ShowWarning("Ups, algo ha salido mal, por favor verifica tus datos.");
                Debug.Log(www.error);
            }
        }
    }

    public void AutoLogin ()
    {
        Debug.Log("AutoLogin");
        if (PlayerPrefs.HasKey("user"))
        {
            email.text = PlayerPrefs.GetString("user");
            pass.text = PlayerPrefs.GetString("pass");
            StartCoroutine(Upload());

        }
    }
    public void SendRegisterPetition()
    {
        StartCoroutine(RegisterService());
    }

    IEnumerator RegisterService()
    {

        WWWForm form = new WWWForm();
        form.AddField("user", emailregister.text);

        UnityWebRequest www = UnityWebRequest.Post("https://bikebi.com.co/restapi/v1/usuarioRegistro", form);
        www.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        www.SetRequestHeader("Authorization", "3d524a53c110e4c22463b10ed32cef9d");
        WarningManager.Instance.ShowLoading();
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            var register = JsonConvert.DeserializeObject<RegisterResponse>(www.downloadHandler.text);
            WarningManager.Instance.ShowWarning(register.status);
        }
        else
        {
            try
            {
                var register = JsonConvert.DeserializeObject<RegisterResponse>(www.downloadHandler.text);
                WarningManager.Instance.ShowWarning(HttpUtility.HtmlDecode(register.status));
            }
            catch (Exception e)
            {
                WarningManager.Instance.ShowWarning("Ups, algo ha salido mal, por favor verifica tus datos.");
                Debug.Log(e);
            }
        }
    }
}
