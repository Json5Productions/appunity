﻿using UnityEngine;

public class LookAt : MonoBehaviour
{
    void Update()
    {
        if (Camera.main)
        {
            transform.LookAt(Camera.main.transform);
        }
    }
}
