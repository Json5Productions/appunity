﻿using System.Collections.Generic;
using UnityEngine;

public class NewAnimationControlerMenuPrincipal2 : MonoBehaviour
{
    [SerializeField]
    private Animator animator;
    [SerializeField]
    private string state;
    private bool open;
    [SerializeField]
    private List<GameObject> images;

    public List<GameObject> Images { get => images; set => images = value; }

    public void ShowMenu(GameObject image)
    {
        if (animator)
            animator.SetBool(state, true);
        foreach (var img in Images)
        {
            if (img == image)
                img.SetActive(true);
            else
                img.SetActive(false);
        }
    }
    public void HideMenu(GameObject image)
    {
        if (animator)
            animator.SetBool(state, false);
        foreach (var img in Images)
        {
            if (img == image)
                img.SetActive(true);
            else
                img.SetActive(false);
        }
    }

}
