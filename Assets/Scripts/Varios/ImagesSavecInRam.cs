﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImagesSavecInRam : MonoBehaviour
{
    public List<ImagesStored> imagesStoreds = new List<ImagesStored>();
    public static ImagesSavecInRam Instance;

    void Awake()
    {
        Instance = this;
    }
}

public class ImagesStored
{
    public string idimage;
    public Sprite sprite;

    public ImagesStored(string idimage, Sprite sprite)
    {
        this.idimage = idimage;
        this.sprite = sprite;
    }
}