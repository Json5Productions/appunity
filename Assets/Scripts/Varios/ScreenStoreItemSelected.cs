﻿using DoozyUI;
using Newtonsoft.Json;
using QuickEngine.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ScreenStoreItemSelected : MonoBehaviour
{
    [SerializeField]
    private string propietyToEnableAR = "AR_ENABLE";
    [SerializeField]
    private ResponseSpecificProduct currentSpecificProduct;
    [SerializeField]
    private GameObject prefabButtonShowMore;
    [SerializeField]
    private PrefabItemStorepreview item;
    [SerializeField]
    private Image image;
    [SerializeField]
    private Text precioText, tipoText, nombreText, ModeloText;
    [SerializeField]
    private UIButton buttonTienda, button3d, buttonRa;
    [SerializeField]
    private string urlWeb;
    [SerializeField]
    private DetailsItemScreen detailsScreen;
    [SerializeField]
    private AssetBundleInstantiatorExample assetBundleInstantiator;
    [SerializeField]
    private LoginPeticion loginPeticion;
    [SerializeField]
    private List<ResponseSpecificProduct> favorites;
    [SerializeField]
    private UnityEvent OnSetedPrincipalImage;
    [SerializeField]
    private PrefabSubCategorie prefabSubCategorie;
    [SerializeField]
    private Transform paretnSubCategorie;
    [SerializeField]
    private PrefabItemList prefabItemList;
    [SerializeField]
    private Transform parentItemList;

    private bool showMore, Only3d;

    private ResponseProducts products;
    private int currentPage = 0, tempCategori;
    public static ScreenStoreItemSelected Instace;

    public ResponseSpecificProduct CurrentSpecificProduct { get => currentSpecificProduct; set => currentSpecificProduct = value; }
    public int TempCategori { get => tempCategori; set => tempCategori = value; }

    private void Awake()
    {
        System.Globalization.CultureInfo info = System.Globalization.CultureInfo.GetCultureInfo("es-CO");
        if (ScreenStoreItemSelected.Instace == null)
        {
            ScreenStoreItemSelected.Instace = this;
        }
        else
            Destroy(this);
    }

    public void UpdateCurrentProduct(ResponseSpecificProduct product, Sprite img)
    {
        currentSpecificProduct = product;
        if (TempCategori != CurrentSpecificProduct.row.categoria)
        {
            TempCategori = CurrentSpecificProduct.row.categoria;
            GetProductsInCurrentPage();
        }
        ShowProduct(img);
    }

    public void GetSpecifidProduct(int idProduct)
    {
        StartCoroutine(DoPeticionToBack(idProduct));
    }

    private IEnumerator DoPeticionToBack(int product)
    {
        yield return new GetSpecifitProductServiceData(product).SendAsync(Response);
    }
    private void Response(string response, string code)
    {
        if (code.Contains("200"))
        {
            this.CurrentSpecificProduct = JsonConvert.DeserializeObject<ResponseSpecificProduct>(response);
            if (TempCategori != CurrentSpecificProduct.row.categoria)
            {
                TempCategori = CurrentSpecificProduct.row.categoria;
                GetProductsInCurrentPage();
            }
            ShowProduct();
        }
    }

    public void On3dButtonPress()
    {
        assetBundleInstantiator.BundleToInstantiate.BundleName = CurrentSpecificProduct.row.id.ToString();
        assetBundleInstantiator.DownloadTestBundle();
    }
    public void ShowProduct()
    {
        var pricingstr = string.Format("{0:c0}", Convert.ToDecimal(CurrentSpecificProduct.row.valor));
        pricingstr = pricingstr.Replace(pricingstr[0], '$');
        pricingstr = pricingstr.Insert(1, " ");
        precioText.text = $"<color=red><b>Precio : </b></color>{pricingstr}";
        tipoText.text = $"<color=red><b>Tipo : </b></color>{GetCategories.Instance.ReturnNameCategoriebyIndex(CurrentSpecificProduct.row.categoria)}";
        nombreText.text = $"<b><color=red>{CurrentSpecificProduct.row.producto.ToUpper()}</color></b>";
        ModeloText.text = $"<b><color=red>Modelo : </color></b>{CurrentSpecificProduct.row.modelo.ToUpper()}";
        if (CurrentSpecificProduct.row.referencia3 != propietyToEnableAR)
        {
            buttonRa.gameObject.SetActive(false);
            button3d.gameObject.SetActive(false);
        }
        else
        {
            buttonRa.gameObject.SetActive(true);
            button3d.gameObject.SetActive(true);

        }
        var downloader = gameObject.AddComponent<DownloaderImages>();
        downloader.RawImage = image;
        downloader.onSetImage += () => OnSetedPrincipalImage.Invoke();
        if (!string.IsNullOrEmpty(CurrentSpecificProduct.row.imagenes.imagenes[0]))
            downloader.DownloadImage(CurrentSpecificProduct.row.imagenes.imagenes[0]);
        else
            downloader.ActiveLoadingByStatus(true);
    }
    public void ShowProduct(Sprite img)
    {
        var pricingstr = string.Format("{0:c0}", Convert.ToDecimal(CurrentSpecificProduct.row.valor));
        pricingstr = pricingstr.Replace(pricingstr[0], '$');
        pricingstr = pricingstr.Insert(1, " ");
        precioText.text = $"<color=red><b>Precio : </b></color>{pricingstr}";
        tipoText.text = $"<color=red><b>Tipo : </b></color>{GetCategories.Instance.ReturnNameCategoriebyIndex(CurrentSpecificProduct.row.categoria)}";
        nombreText.text = $"<b><color=red>{CurrentSpecificProduct.row.producto.ToUpper()}</color></b>";
        ModeloText.text = $"<b><color=red>Modelo : </color></b>{CurrentSpecificProduct.row.modelo.ToUpper()}";
        if (CurrentSpecificProduct.row.referencia3 != propietyToEnableAR)
        {
            buttonRa.gameObject.SetActive(false);
            button3d.gameObject.SetActive(false);
        }
        else
        {
            buttonRa.gameObject.SetActive(true);
            button3d.gameObject.SetActive(true);

        }
        image.sprite = img;
        OnSetedPrincipalImage.Invoke();
    }

    public void SetNewCategorie(int cat)
    {
        TempCategori = cat;
        currentPage = 0;
        showMore = true;
        Only3d = false;
        foreach (Transform transform in parentItemList)
        {
            Destroy(transform.gameObject);
        }
    }

    public void GetProductsInCurrentPage()
    {
        Debug.Log("ESTA PAGINAAA " + currentPage);
        if (!Only3d)
            StartCoroutine(DoPeticionToBackProducts(TempCategori));
        else
            SearchAll3dProducts();
    }
    public void Vermas()
    {
        GetProductsInCurrentPage();
    }
    private IEnumerator DoPeticionToBackProducts(int categoria)
    {
        Only3d = false;
        yield return new ProductosServiceData($"categoria={categoria}&offset={currentPage * 10}&limit=10").SendAsync(ResponseCategorias);
    }
    private IEnumerator DoPeticionToBackforSubCats(int categoria)
    {
        yield return new ProductosServiceData($"categoria={categoria}&offset={currentPage * 10}&limit=1").SendAsync(ResponseSubCategorias);
    }
    private void ResponseSubCategorias(string response, string code)
    {
        if (code.Contains("200"))
        {
            products = JsonConvert.DeserializeObject<ResponseProducts>(response);
            InstantiateSubCategories();
        }
    }

    public void InstantiateSubCategories()
    {
        foreach (var cat in products.subcategoria)
        {
            if (cat != products.categoria)
                InstantiateItemCat(GetCategories.Instance.ReturnNameCategoriebyIndex(cat), cat);
        }
    }

    private void InstantiateItemCat(string title, int cat)
    {
        var subCat = Instantiate(prefabSubCategorie, paretnSubCategorie);
        string id = null;
        if (ImagesManager.Instance.Data.Find(x => x.idImage == title) != null)
        {
            id = ImagesManager.Instance.Data.Find(x => x.idImage == title).urlImage;
        }
        subCat.SetupItem(title, id, cat);
    }

    public void SendMessageToWhatsapp()
    {
        var message = $"https://wa.me/+573203643344/?text= Hola!, Me gustaría recibir mas información acerca de este producto: \n{urlWeb + CurrentSpecificProduct.row.id}";
        Application.OpenURL(message);
    }

    private void ResponseCategorias(string response, string code)
    {
        if (code.Contains("200"))
        {
            Debug.Log("pagination: " + currentPage);
            currentPage++;
            try
            {
                products = JsonConvert.DeserializeObject<ResponseProducts>(response);
            }
            catch (Exception e)
            {
                var newJson = response.Replace("[]", "null");
                products = JsonConvert.DeserializeObject<ResponseProducts>(newJson);

            }
            if (products.rows.Count < 10)
                showMore = false;

            InstantiateItemList();
        }
    }

    private void InstantiateItemList()
    {
        foreach (var item in products.rows)
        {
            var itemList = Instantiate(prefabItemList, parentItemList);
            itemList.SetupItem(item);
        }

        if (showMore)
            Instantiate(prefabButtonShowMore, parentItemList);
    }
    public void SearchFavoritesItems()
    {
        if (loginPeticion.isLoged)
            FavoritesPeticion();
        else
            WarningManager.Instance.ShowWarning("Ups, primero debes iniciar sesion para poder ver tus favoritos.");
    }

    public void FavoritesPeticion()
    {
        CurrentSpecificProduct = null;
        foreach (var fav in loginPeticion.UserData.favorites)
        {
            StartCoroutine(DoPeticionToBackProductsFavorites(fav.id));
        }
    }

    private IEnumerator DoPeticionToBackProductsFavorites(int product)
    {
        yield return new GetSpecifitProductServiceData(product).SendAsync(ResponseFavorites);
    }
    private void ResponseFavorites(string response, string code)
    {
        if (code.Contains("200"))
        {
            var item = JsonConvert.DeserializeObject<ResponseSpecificProduct>(response);
            var goInstantiate = Instantiate(prefabItemList, parentItemList);
            goInstantiate.SetupItem(item.row);
        }
    }
    public void SearchAll3dProducts()
    {
        Only3d = true;
        StartCoroutine(DoPeticionToBackProductsOnly3d());
    }
    private IEnumerator DoPeticionToBackProductsOnly3d()
    {
        yield return new ProductosServiceData($"search={propietyToEnableAR}&offset={currentPage * 10}&limit=10").SendAsync(ResponseCategorias);
    }
    public void SendToWebProduct()
    {
        Debug.Log(CurrentSpecificProduct.row.id);
        Application.OpenURL(urlWeb + CurrentSpecificProduct.row.id);
    }

    public void DetailsCurrentItem()
    {
        detailsScreen.CurrentProduct = this.CurrentSpecificProduct.row;
        detailsScreen.LoadCurrentProduct();
    }

    public void SearchSpecificCategory(string Cat)
    {
        var category = GetCategories.Instance.ReturnIndexCategoriebyName(Cat);
        CurrentSpecificProduct = null;
        foreach (Transform transform in paretnSubCategorie)
        {
            Destroy(transform.gameObject);
        }
        StopAllCoroutines();
        if (category != -1)
        {
            currentPage = 0;
            StartCoroutine(DoPeticionToBackforSubCats(category));
        }
    }
}
