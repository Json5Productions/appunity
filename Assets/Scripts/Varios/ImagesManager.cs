﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class ImagesManager : MonoBehaviour
{
    [SerializeField]
    private List<ImagesInfo> imagesInfos;
    [SerializeField]
    private string urlData;

    public List<ImageID> Data;

    public static ImagesManager Instance;

    private void Start()
    {
        Instance = this;
        StartCoroutine(DownlaodData());
    }
    IEnumerator DownlaodData()
    {
        UnityWebRequest www = UnityWebRequest.Get(urlData);
        yield return www.SendWebRequest();

        if (string.IsNullOrEmpty(www.error))
        {
            Data = JsonConvert.DeserializeObject<List<ImageID>>(www.downloadHandler.text);
            OnReceiveData();
        }
    }

    public void OnReceiveData()
    {
        foreach (var dat in Data)
        {
            if (!dat.idImage.Contains("Screen"))
            {
                var downloader = gameObject.AddComponent<DownloaderImages>();
                var datInScene = imagesInfos.Find(x => x.imageID.idImage == dat.idImage);
                if (datInScene != null)
                {
                    downloader.RawImage = datInScene.image;
                    if (!string.IsNullOrEmpty(dat.urlImage))
                        downloader.DownloadImage(dat.urlImage);
                    else
                        downloader.ActiveLoadingByStatus(true);
                }
            }
            else
            {
                var downloader = gameObject.AddComponent<DownloaderImages>();
                var datInScene = imagesInfos.Find(x => x.imageID.idImage == dat.idImage);
                if (!string.IsNullOrEmpty(dat.urlImage))
                    downloader.DownloadTexture(dat.urlImage, datInScene.Mat);
            }
        }
    }

    [EasyButtons.Button]
    public void CreateJson()
    {
        var data = new List<ImageID>();
        foreach (var dat in imagesInfos)
        {
            data.Add(dat.imageID);
        }
        Debug.Log(JsonConvert.SerializeObject(data));

    }

}

[System.Serializable]
public class ImagesInfo
{
    public Image image;
    public Material Mat;
    public ImageID imageID;
    public Sprite sprite;
}

[System.Serializable]
public class ImageID
{
    public string idImage;
    public string urlImage;
}