﻿using DoozyUI;
using UnityEngine;
using UnityEngine.UI;
public class PrefabDestacados : MonoBehaviour
{
    [SerializeField]
    private Image image;
    [SerializeField]
    private Product product;
    [SerializeField]
    private UIButton button;
    public Product Product { get => product; set => product = value; }

    public void SetupPrefab (string urlImage)
    {
        button.OnClick.AddListener(OnCLick);
        var downloader = gameObject.AddComponent<DownloaderImages>();
        downloader.RawImage = image;
        if (!string.IsNullOrEmpty(urlImage))
            downloader.DownloadImage(urlImage);
        else
            downloader.ActiveLoadingByStatus(true);
    }
    public void OnCLick()
    {
        ScreenStoreItemSelected.Instace.GetSpecifidProduct(product.id);
        AnimationControlerMenuPrincipal.Instance.ShowMenu(AnimationControlerMenuPrincipal.Instance.Images[0]);
    }
}
