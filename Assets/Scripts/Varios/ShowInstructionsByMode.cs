﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowInstructionsByMode : MonoBehaviour
{
    [SerializeField]
    private ModeAppManager modeApp;
    public GameObject Ar, Td;

    public void ActiveInstrutionsByMode ()
    {
        if (modeApp.CurrentMode == ModeApp.Ar)
        {
            Ar.SetActive(true);
            Td.SetActive(false);
        }else
        {
            Ar.SetActive(false);
            Td.SetActive(true);
        }
    }
}
