﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerPartsIndividuals : MonoBehaviour
{
    [SerializeField]
    private Transform parent;
    [SerializeField]
    private List<GameObject> parts;
    [SerializeField]
    private ChangePartsBasic changeParts;
    [SerializeField]
    private AssetBundleInstantiatorExample assetBundle;

    public void SetIndivudualParts ()
    {
        foreach (Transform obj in assetBundle.InstancedObjectBundle.transform)
        {
            var itemGroup = assetBundle.CurrentObjectInfo.CompatibleOjects.Find(x => x.NameItem.Equals(obj.name));
            if (itemGroup == null)
            {
                var clone = Instantiate(obj, parent);
                clone.localPosition = Vector3.zero;
                clone.gameObject.SetActive(false);
                parts.Add(clone.gameObject);
                var part = new PartsToEnable();
                part.NameParts = obj.name;
                part.listObjects.Add(clone.gameObject);
                changeParts.List.Add(part);
                changeParts.AllParts.Add(clone.gameObject);
            }
        }
        changeParts.ResetIndex();
    }
}
