﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlerBicis : MonoBehaviour
{
    [SerializeField]
    private List<ChangePartsBasic> bicis;

    private int currentIndex;


    public void SetCurrentBici ()
    {
        bicis[currentIndex].Bicicleta.SetActive(true);
        bicis[currentIndex].ResetIndex();
    }

    public void NextBici ()
    {
        if (currentIndex < bicis.Count)
            currentIndex++;
        else
            currentIndex = 0;
        SetCurrentBici();
    }
}
