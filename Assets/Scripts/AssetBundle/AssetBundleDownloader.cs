﻿using System.IO;
using UnityEngine;

public abstract class AssetBundleDownloader : MonoBehaviour
{
    private AssetBundleRequest bundleRequest;

    public void Setup(AssetBundleRequest bundleRequest)
    {
        this.bundleRequest = bundleRequest;
        this.bundleRequest.OnAssetBundleDownloaded = OnDownloadedSuccess;
        this.bundleRequest.OnAssetBundleFailed = OnDownloadFailed;
    }

    public void Setup(string bundleName, uint bundleVersion)
    {
        bundleRequest = new AssetBundleRequest(bundleName, bundleVersion, OnDownloadedSuccess, OnDownloadFailed);
    }

    public void DownloadRequest()
    {
        AssetBundleManager.Instance.Download(bundleRequest);
    }

    public bool AlreadyHaveBundle ()
    {
        string filePath = Application.persistentDataPath + $"/{bundleRequest.BundleName}.bundle";
        if (File.Exists(filePath))
            return true;
        else
            return false;
    }
    public bool AlreadyHaveBundle (int id)
    {
        string filePath = Application.persistentDataPath + $"/{id}.bundle";
        if (File.Exists(filePath))
            return true;
        else
            return false;
    }

    protected abstract void OnDownloadedSuccess(Object asset);
    protected abstract void OnDownloadFailed();
}
