﻿using System;

[Serializable]
public class AssetBundleRequest 
{
    public string BundleName;
    public uint BundleVersion;
    public Action<UnityEngine.Object> OnAssetBundleDownloaded;
    public Action OnAssetBundleFailed;

    public AssetBundleRequest()
    {
    }
    public AssetBundleRequest(string bundleName, uint bundleVersion, Action<UnityEngine.Object> successCallback, Action failedCallback)
    {
        BundleName = bundleName;
        BundleVersion = bundleVersion;
        OnAssetBundleDownloaded = successCallback;
        OnAssetBundleFailed = failedCallback;
    }
}