﻿using DoozyUI;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class AssetBundleInstantiatorExample : AssetBundleDownloader
{
    [SerializeField]
    private Text loadinText, priceText, descripcionText;
    [SerializeField]
    private Image barProgress;
    [SerializeField]
    private AssetBundleRequest bundleToInstantiate;
    [SerializeField]
    private AssetBundle AssetBundleToInstantiate;
    [SerializeField]
    private UnityEvent onFinishLoadAsset;
    public AssetBundleRequest BundleToInstantiate { get => bundleToInstantiate; set => bundleToInstantiate = value; }
    public AssetBundle AssetBundleToInstantiate1 { get => AssetBundleToInstantiate; set => AssetBundleToInstantiate = value; }
    public GameObject InstancedObjectBundle { get => instancedObjectBundle; set => instancedObjectBundle = value; }
    public ObjectInfo CurrentObjectInfo { get => currentObjectInfo; set => currentObjectInfo = value; }

    [SerializeField]
    private Transform parentGroupsType, parentObject;
    [SerializeField]
    private ChangePartsBasic ConfigParts, configPartsIndividualGroup, ChangePartsSuspension,ChangeParts;
    [SerializeField]
    private ObjectInfo currentObjectInfo;
    [SerializeField]
    private UnityEvent onInstantiateBici, onInstantiateObject;
    [SerializeField]
    private List<ResponseSpecificProduct> products;
    private bool isloading = false;
    [SerializeField]
    private GameObject instancedObjectBundle, tempBiciCompleta, tempIndividualsPartsbici;
    private AssetBundleCreateRequest asset;

    private decimal currentPrice, pricenewPiece, priceOldPiece;

    [EasyButtons.Button]
    public void DownloadTestBundle()
    {
        Setup(BundleToInstantiate);
        AssetBundle.UnloadAllAssetBundles(true);
        if (!AlreadyHaveBundle())
        {
            descripcionText.text = "Descargando componentes, esto SÓLO SUCEDERÁ UNA VEZ por producto, por favor espere.";
            DownloadRequest();
        }
        else
        {
            descripcionText.text = "Cargando el componente, por favor espera.";
            isloading = true;
            string filePath = Application.persistentDataPath + $"/{BundleToInstantiate.BundleName}.bundle";
            UINavigation.ShowUiElement("DownloadingProgress", "3d", false);
            asset = AssetBundle.LoadFromFileAsync(filePath);
        }
    }

    public void OnLadBundle()
    {
        onFinishLoadAsset.Invoke();
    }
    private void Update()
    {
        if (isloading)
        {
            barProgress.fillAmount = asset.progress;
            loadinText.text = (asset.progress * 100).ToString("0") + "%";
            if (asset.isDone)
            {
                isloading = false;
                UINavigation.HideUiElement("DownloadingProgress", "3d", false);
                OnDownloadedSuccess(asset.assetBundle);
            }
        }
    }

    protected override void OnDownloadedSuccess(UnityEngine.Object asset)
    {
        if (InstancedObjectBundle)
            Destroy(InstancedObjectBundle);
        ConfigParts.ClearAllData();
        ChangePartsSuspension.ClearAllData();
        configPartsIndividualGroup.ClearAllData();
        UINavigation.ShowUiElement("instruccions", "3d", false);
        AssetBundleToInstantiate1 = (AssetBundle)asset;
        CurrentObjectInfo = JsonConvert.DeserializeObject<ObjectInfo>(AssetBundleToInstantiate1.LoadAsset<TextAsset>(BundleToInstantiate.BundleName).text);
        switch (currentObjectInfo.item.typeObject)
        {
            case TypeObject.bicicleta:
                InstancedObjectBundle = Instantiate(AssetBundleToInstantiate1.mainAsset as GameObject, parentGroupsType);
                onInstantiateBici.Invoke();
                SetupPartsGroups();
                break;
            case TypeObject.parte:
                InstancedObjectBundle = Instantiate(AssetBundleToInstantiate1.mainAsset as GameObject, parentObject);
                onInstantiateObject.Invoke();
                break;
            case TypeObject.grupo:
                InstancedObjectBundle = Instantiate(AssetBundleToInstantiate1.mainAsset as GameObject, parentObject);
                onInstantiateObject.Invoke();
                break;
        }
        OnLadBundle();
        FindObjectOfType<FollowTagComponent>().SetTarget();

    }

    public void Reset3dObject()
    {
        instancedObjectBundle.SetActive(true);
        instancedObjectBundle.transform.parent = parentGroupsType;
        parentGroupsType.transform.position = new Vector3(-0.266f, 2.148f, -0.388f);
        instancedObjectBundle.transform.localPosition = new Vector3(0, -0.15f, 0.48f);
        instancedObjectBundle.transform.localRotation = Quaternion.Euler(Vector3.zero);
    }

    public void ShowMedidas()
    {
        foreach (Transform obj in InstancedObjectBundle.transform)
        {
            if (obj.CompareTag("medidas"))
            {
                if (obj.gameObject.activeSelf)
                    obj.gameObject.SetActive(false);
                else
                    obj.gameObject.SetActive(true);
            }
        }
    }

    public void SetItemsIndividualsGroups()
    {
        InstancedObjectBundle = tempIndividualsPartsbici;
        tempIndividualsPartsbici.SetActive(true);
        tempBiciCompleta.SetActive(false);
    }
    public void SetGroupItems()
    {
        InstancedObjectBundle = tempBiciCompleta;
        if (tempIndividualsPartsbici)
            tempIndividualsPartsbici.SetActive(false);
        if (tempBiciCompleta)
            tempBiciCompleta.SetActive(true);
    }

    private void SetupPartsGroups()
    {
        if (tempBiciCompleta)
            Destroy(tempBiciCompleta);
        tempBiciCompleta = InstancedObjectBundle;
        foreach (Transform obj in InstancedObjectBundle.transform)
        {
            obj.gameObject.SetActive(true);
        }

        foreach (var obj in CurrentObjectInfo.CompatibleOjects)
        {
            var part = new PartsToEnable();
            var partSusp = new PartsToEnable();
            part.NameParts = obj.NameItem;
            GameObject childObj = null;
            GameObject suspObj = null;
            foreach (Transform child in InstancedObjectBundle.transform)
            {
                if (child.name.Contains(obj.NameItem) && obj.typeObject == TypeObject.grupo)
                {
                    childObj = child.gameObject;
                    child.gameObject.SetActive(false);
                    break;
                }
                if (child.name.Contains(obj.NameItem) && obj.typeObject == TypeObject.suspension)
                {
                    suspObj = child.gameObject;
                    break;
                }
            }
            if (childObj)
            {
                part.listObjects.Add(childObj);
                ConfigParts.List.Add(part);
                ConfigParts.AllParts.Add(childObj);
            }
            else if (suspObj)
            {
                partSusp.listObjects.Add(suspObj);
                ChangePartsSuspension.List.Add(partSusp);
                ChangePartsSuspension.AllParts.Add(suspObj);
            }
        }
        SetupPartsInducidualInGroup();
        SetupMedidas();
        SetupPrice();
        ConfigParts.ResetIndex();
    }

    public void SetupPrice()
    {
        products.Clear();
        currentPrice = 0;
        foreach (var item in currentObjectInfo.CompatibleOjects)
        {
            try
            {
                //  GetSpecifidProduct(int.Parse(item.IdItem));
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }
    }

    public void RefreshPrice(string id)
    {
        if (currentPrice == 0)
        {
            currentPrice = decimal.Parse(ScreenStoreItemSelected.Instace.CurrentSpecificProduct.row.valor);
            try
            {
                priceOldPiece = int.Parse(products.Find(x => x.id == currentObjectInfo.CompatibleOjects[0].IdItem).id);
            }
            catch (Exception e)
            {

            }
        }
        else
        {
            try
            {
                priceOldPiece = pricenewPiece;
                pricenewPiece = decimal.Parse(products.Find(x => x.id == id).row.valor);
                currentPrice = currentPrice - priceOldPiece;
                currentPrice = currentPrice + pricenewPiece;
            }
            catch (Exception e)
            {
                Debug.Log(e + currentPrice.ToString());
            }
        }
        var pricingstr = string.Format("{0:c0}", Convert.ToDecimal(currentPrice.ToString()));
        pricingstr = pricingstr.Replace(pricingstr[0], '$');
        pricingstr = pricingstr.Insert(1, " ");
    }

    public void GetSpecifidProduct(int idProduct)
    {
        StartCoroutine(DoPeticionToBack(idProduct));
    }

    private IEnumerator DoPeticionToBack(int product)
    {
        yield return new GetSpecifitProductServiceData(product).SendAsync(Response);
    }
    private void Response(string response, string code)
    {
        if (code.Contains("200"))
        {
            var prod = JsonConvert.DeserializeObject<ResponseSpecificProduct>(response);
            products.Add(prod);
            /* if (products.Count >= currentObjectInfo.CompatibleOjects.Count)
             {
                 RefreshPrice("none");
             }*/
        }
    }

    private void SetupMedidas()
    {
        var texts = GameObject.FindGameObjectsWithTag("TextMedida");
        foreach (var t in texts)
        {
            t.AddComponent(typeof(LookAt));
        }
        ShowMedidas();
    }

    private void SetupPartsInducidualInGroup()
    {
        if (tempIndividualsPartsbici != null)
            Destroy(tempIndividualsPartsbici);
        tempIndividualsPartsbici = GameObject.FindGameObjectWithTag("Individual");
        foreach (Transform obj in tempIndividualsPartsbici.transform)
        {
            var part = new PartsToEnable();
            part.NameParts = obj.gameObject.name;
            part.listObjects.Add(obj.gameObject);
            configPartsIndividualGroup.List.Add(part);
            configPartsIndividualGroup.AllParts.Add(obj.gameObject);
        }
        tempIndividualsPartsbici.transform.parent = parentObject;
        tempIndividualsPartsbici.SetActive(false);
        configPartsIndividualGroup.ResetIndex();
    }

    protected override void OnDownloadFailed()
    {
        Debug.LogError("Download failed!");
    }
}
