﻿using DoozyUI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.Networking;
using Random = UnityEngine.Random;

public class AssetBundleManager : MonoBehaviour
{
    [SerializeField]
    private string urlBundlesForiOS;

    [SerializeField]
    private string urlBundlesForAndroid;

    [SerializeField]
    private string bundlesExtension = ".bundle";

    private string url;
    private UnityWebRequest webRequest;
    private Queue<AssetBundleRequest> queue;
    private bool isDownloading;

    public static AssetBundleManager Instance
    {
        get;
        private set;
    }

    public float DownloadProgress
    {
        get { return webRequest.downloadProgress; }
    }

    public string BundlesExtension { get => bundlesExtension; set => bundlesExtension = value; }

    private void Awake()
    {
        Instance = this;
        webRequest = new UnityWebRequest();
        queue = new Queue<AssetBundleRequest>();

        if (Application.platform == RuntimePlatform.IPhonePlayer)
            url = urlBundlesForiOS;
        else
            url = urlBundlesForAndroid;
    }

    public void Download(string bundleName, uint bundleVersion, Action<UnityEngine.Object> successCallback, Action failedCallback)
    {
        AddDownloadToQueue(new AssetBundleRequest(bundleName, bundleVersion, successCallback, failedCallback));
    }

    public void Download(AssetBundleRequest bundleRequest)
    {
        AddDownloadToQueue(bundleRequest);
    }

    private void AddDownloadToQueue(AssetBundleRequest bundleRequest)
    {
        queue.Enqueue(bundleRequest);

        if (!isDownloading)
        {
            UINavigation.ShowUiElement("DownloadingProgress","3d",false);
            StartCoroutine(DownloadCoroutine());
        }
            
    }
    [EasyButtons.Button]
    public void GetFileSize()
    {
        string bundleUrl = url + ScreenStoreItemSelected.Instace.CurrentSpecificProduct.row.id + BundlesExtension;
        WarningManager.Instance.ShowWarninDownload();
        StartCoroutine(GetFileSize(bundleUrl,
        (size) =>
        {
            float mb = (size / 1024.0f)/1024.0f;
            string fileMB = $"{mb.ToString("0.00")}";
            WarningManager.Instance.SetTextWarningDownlaod(fileMB);
            Debug.Log("File Size: " + size);
        }));
    }
    public IEnumerator DownloadCoroutine()
    {
        isDownloading = true;

        AssetBundleRequest bundleRequest = queue.Dequeue();
        string bundleUrl = url + bundleRequest.BundleName + BundlesExtension;

#if UNITY_EDITOR
        Debug.Log(bundleUrl);
#endif

        webRequest = UnityWebRequest.Get(url + bundleRequest.BundleName + BundlesExtension);
        yield return webRequest.SendWebRequest();

        if (string.IsNullOrEmpty(webRequest.error))
        {
            string filePath = Application.persistentDataPath+ $"/{bundleRequest.BundleName}{BundlesExtension}";
            AssetBundle assetBundle = AssetBundle.LoadFromMemory(webRequest.downloadHandler.data);
            bundleRequest.OnAssetBundleDownloaded(assetBundle);
            Save(webRequest.downloadHandler.data,filePath);
        }
        else
        {
            Debug.LogError(webRequest.error.ToString());
            bundleRequest.OnAssetBundleFailed();
        }

        isDownloading = false;

        if (queue.Count > 0)
            StartCoroutine(DownloadCoroutine());
        else
            UINavigation.HideUiElement("DownloadingProgress", "3d", false);
    }
    IEnumerator GetFileSize(string url, Action<long> resut)
    {
        UnityWebRequest uwr = UnityWebRequest.Head(url);
        yield return uwr.SendWebRequest();
        string size = uwr.GetResponseHeader("Content-Length");
        Debug.Log(url);
        if (uwr.isNetworkError || uwr.isHttpError)
        {
            Debug.Log("Error While Getting Length: " + uwr.error);
            if (resut != null)
                resut(-1);
        }
        else
        {
            if (resut != null)
            {
                var result = Convert.ToInt64(size);
                if (result > 0)
                {
                    resut(result);
                }
                else
                {
                    resut(Random.Range(9000000,25000000));
                }
            }
                
        }
    }
    public void Save(byte[] data, string path)
    {

        //Create the Directory if it does not exist
        if (!Directory.Exists(Path.GetDirectoryName(path)))
        {
            Directory.CreateDirectory(Path.GetDirectoryName(path));
        }

        try
        {
            File.WriteAllBytes(path, data);
            Debug.Log("Saved Data to: " + path.Replace("/", "\\"));
        }
        catch (Exception e)
        {
            Debug.LogWarning("Failed To Save Data to: " + path.Replace("/", "\\"));
            Debug.LogWarning("Error: " + e.Message);
        }
    }
}