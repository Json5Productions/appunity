#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;

public class AssetBundleCreator : EditorWindow
{
    [SerializeField]
    private string extension = "bundle";

    private string path;


    [MenuItem("Window/Asset Bundle Creator")]
    public static void Init()
    {
        GetWindow(typeof(AssetBundleCreator), false, "Asset Bundle Creator");
    }

    private void OnGUI()
    {
        GUILayout.Space(10F);
        GUI.Label(new Rect(0, 7, 65, 18), " Extension: ");
        extension = GUI.TextField(new Rect(75, 5, 175, 18), extension, 25);

        GUILayout.Space(20F);
        if (GUILayout.Button("Clear Player Prefs"))
        {
            if (EditorUtility.DisplayDialog("Clear Data", "Are you sure that you wish clear app's data?", "Yes", "No"))
                PlayerPrefs.DeleteAll();
        }

        GUILayout.Space(5F);
        if (GUILayout.Button("Clear Asset Bundles Cache"))
        {
            if (EditorUtility.DisplayDialog("Asset Bundle Clear Cache", "Are you sure that you wish clear cache?", "Yes", "No"))
                Caching.ClearCache();
        }

        GUILayout.Space(10F);
        if (GUILayout.Button("Convert Prefab to Asset Bundle for IOS"))
            CreateAssetBundle(BuildTarget.iOS);

        GUILayout.Space(5F);
        if (GUILayout.Button("Convert Prefab to Asset Bundle for Android"))
            CreateAssetBundle(BuildTarget.Android);

        GUILayout.Space(5F);
        if (GUILayout.Button("Convert ALL PrefabS to Asset Bundle for Android"))
            CreateAllAssetsBundles(BuildTarget.Android,"Android");
        GUILayout.Space(5F);
        if (GUILayout.Button("Convert ALL PrefabS to Asset Bundle for IOS"))
            CreateAllAssetsBundles(BuildTarget.iOS,"Ios");

        GUILayout.Space(5F);
        if (GUILayout.Button("Convert Prefab to Asset Bundle for Web"))
            CreateAssetBundle(BuildTarget.WebGL);
    }

    private void CreateAssetBundle(BuildTarget buildTarget)
    {
        var id = FindObjectOfType<JsonInfoSaver>();
        if (Selection.activeObject != null)
        {
            path = EditorUtility.SaveFilePanel("Create new Asset Bundle", "", id.Info.item.IdItem, extension);
            CreatePrefab(buildTarget);
        }
        else
            Debug.LogWarning("Nothing to convert.");
    }

    private void CreatePrefab(BuildTarget target)
    {
        if (path.Length != 0 && AssetDatabase.Contains(Selection.activeObject))
        {
            var Objects = new List<Object>();
            Object[] selection = Selection.GetFiltered(typeof(Object), SelectionMode.Unfiltered);
            Objects = Selection.objects.ToList();
            var id = FindObjectOfType<JsonInfoSaver>();
            var text = Resources.Load("JsonInfo/" +
                id.Info.item.IdItem) as TextAsset;
            Objects.Add(text);
            BuildPipeline.BuildAssetBundle(Selection.activeObject, Objects.ToArray(), path, BuildAssetBundleOptions.CollectDependencies | BuildAssetBundleOptions.CompleteAssets | BuildAssetBundleOptions.DeterministicAssetBundle, target);
            Selection.objects = selection;
        }
    }

    private void CreateAllAssetsBundles(BuildTarget buildTarget,string device)
    {
        var ListBundles = FindObjectOfType<PrefabsBundlesSetter>();
        foreach (var bundle in ListBundles.InfoPrefabs)
        {
            var Objects = new List<Object>();
            Objects.Add(bundle.prefab);
            var text = Resources.Load("JsonInfo/" +
                bundle.id) as TextAsset;
            Objects.Add(text);
            BuildPipeline.BuildAssetBundle(bundle.prefab, Objects.ToArray(), $"C:/Users/DIRECT/Desktop/bundles bikebi/{device}/{bundle.id}.bundle", BuildAssetBundleOptions.CollectDependencies | BuildAssetBundleOptions.CompleteAssets | BuildAssetBundleOptions.DeterministicAssetBundle, buildTarget);
            
        }
    }

}
#endif