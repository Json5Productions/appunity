﻿using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

public class PlayersPrefController : MonoBehaviour
{
    [MenuItem("Tools/reset PlayerPrefs")]
    public void ResetPlayerPrefs ()
    {
        PlayerPrefs.DeleteAll();
    }
}
#endif